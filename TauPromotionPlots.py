<<<<<<< HEAD
  # -*- coding: utf-8 -*-
#
# Tau promotion light.
#
# Previous version: compare.py
#
# (85)
#
# $Id: TauPromotionPlots.py 2687 2016-12-21 16:50:11Z eis $


### imports 
import os
import sys
import Alex
import ROOT
import argparse
import AlexROOT
import RatioPlot


### config + cmdline
version = "default"
bins_pt = 60

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', type = int, default = 2)
args = parser.parse_args()
path_orig = None

path = "/project/etp/Samara/SFrameTest/SFrame/Promote/run"
if args.input == 0:
  print "Using DUMMY TREE"
  path_orig = "/home/a/Adam.Samara/DummyTree/Wdummy.root"
  path_inf_meas = os.path.join(path, "Measure.Dummy.v2.root")
  path_inf_prom = os.path.join(path, "Promote.Dummy.v2.root")
  path_outpdf = "TauPromotionPlots_Dummy2.pdf"

elif args.input == 1:
  print "Using Alpgen? TEST"
  path_orig = ""
  path_inf_meas = os.path.join(path, "Measure.MC_OldMGP8_met.v1.root")
  path_inf_prom = os.path.join(path, "Promote.MC_OldMGP8_met.v1.root")
  path_outpdf = "TauPromotionPlots_Alpgen.pdf"
elif args.input == 2:
  print "Using SHERPA TEST"
  path_orig = "/home/a/A.Mann/TauAnalysisCode/1_xAOD/atlas-phys-susy-tau-framework-analysis__NEW/xAODSUSYTauAnalysis/run/SUSYTauAnalysisCycle.364184_Sherpa_Wtaunu.SUSY3.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.v1.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.v1.root")
  path_outpdf = "TauPromotionPlots_Sherpa.pdf"
elif args.input == 3:
  print "Using SHERPA xAOD TEST"
  path_orig = "/home/a/A.Mann/TauAnalysisCode/1_xAOD/atlas-phys-susy-tau-framework-analysis__NEW/xAODSUSYTauAnalysis/run/SUSYTauAnalysisCycle.364184_Sherpa_Wtaunu.xAOD.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.xAOD.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.xAOD.root")
  path_outpdf = "TauPromotionPlots_Sherpa_xAOD.pdf"
elif args.input == 4:
  print "Using SHERPA unbias TEST"
  #path_orig = "/home/a/A.Mann/TauAnalysisCode/1_xAOD/atlas-phys-susy-tau-framework-analysis__NEW/xAODSUSYTauAnalysis/run/SUSYTauAnalysisCycle.364184_Sherpa_Wtaunu.xAOD.root"
  #path_orig = "/project/etp/Samara/xAODSUSYTauAnalysis/xAODSUSYTauAnalysis/config/run/SUSYTauAnalysisCycle.mc15_13TeV.364190.e5340_s2726_r7772_r7676_p2949.root"
  path_origs = \
  ["/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0001.v030-17.SUSYTauAnalysisCycle.mc15_364184.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222975._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0001.v030-17.SUSYTauAnalysisCycle.mc15_364184.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222975._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0001.v030-17.SUSYTauAnalysisCycle.mc15_364184.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222975._000003.SUSYTauAnalysis.root", 
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0001.v030-17.SUSYTauAnalysisCycle.mc15_364184.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222975._000004.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0002.v030-17.SUSYTauAnalysisCycle.mc15_364185.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222979._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0002.v030-17.SUSYTauAnalysisCycle.mc15_364185.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222979._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0003.v030-17.SUSYTauAnalysisCycle.mc15_364186.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222982._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0003.v030-17.SUSYTauAnalysisCycle.mc15_364186.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222982._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0003.v030-17.SUSYTauAnalysisCycle.mc15_364186.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222982._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0003.v030-17.SUSYTauAnalysisCycle.mc15_364186.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222982._000004.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0025.v030-3.SUSYTauAnalysisCycle.mc15_364187.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211404._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0025.v030-3.SUSYTauAnalysisCycle.mc15_364187.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211404._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0025.v030-3.SUSYTauAnalysisCycle.mc15_364187.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211404._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0025.v030-3.SUSYTauAnalysisCycle.mc15_364187.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211404._000004.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0026.v030-3.SUSYTauAnalysisCycle.mc15_364188.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211408._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0026.v030-3.SUSYTauAnalysisCycle.mc15_364188.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211408._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0026.v030-3.SUSYTauAnalysisCycle.mc15_364188.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211408._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0027.v030-3.SUSYTauAnalysisCycle.mc15_364189.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211410._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0027.v030-3.SUSYTauAnalysisCycle.mc15_364189.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211410._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0027.v030-3.SUSYTauAnalysisCycle.mc15_364189.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11211410._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0004.v030-17.SUSYTauAnalysisCycle.mc15_364190.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222986._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0004.v030-17.SUSYTauAnalysisCycle.mc15_364190.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222986._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0004.v030-17.SUSYTauAnalysisCycle.mc15_364190.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222986._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0005.v030-17.SUSYTauAnalysisCycle.mc15_364191.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222994._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0005.v030-17.SUSYTauAnalysisCycle.mc15_364191.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222994._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0005.v030-17.SUSYTauAnalysisCycle.mc15_364191.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222994._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000004.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000005.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000006.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000007.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0006.v030-17.SUSYTauAnalysisCycle.mc15_364192.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11222998._000008.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0007.v030-17.SUSYTauAnalysisCycle.mc15_364193.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223002._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0007.v030-17.SUSYTauAnalysisCycle.mc15_364193.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223002._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0008.v030-17.SUSYTauAnalysisCycle.mc15_364194.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223010._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0008.v030-17.SUSYTauAnalysisCycle.mc15_364194.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223010._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0009.v030-17.SUSYTauAnalysisCycle.mc15_364195.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223016._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0009.v030-17.SUSYTauAnalysisCycle.mc15_364195.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223016._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0010.v030-17.SUSYTauAnalysisCycle.mc15_364196.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223033._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0010.v030-17.SUSYTauAnalysisCycle.mc15_364196.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223033._000002.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0010.v030-17.SUSYTauAnalysisCycle.mc15_364196.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223033._000003.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0010.v030-17.SUSYTauAnalysisCycle.mc15_364196.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223033._000004.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0011.v030-17.SUSYTauAnalysisCycle.mc15_364197.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223041._000001.SUSYTauAnalysis.root",
   "/project/etp/Samara/xAODSUSYTauAnalysis/samples_onlyTaupromOutput/user.asamara.0011.v030-17.SUSYTauAnalysisCycle.mc15_364197.e5340_s2726_r7772_r7676_p2949_SUSYTauAnalysis.root/user.asamara.11223041._000002.SUSYTauAnalysis.root"]
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.mc15_221_Wtaunu.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.mc15_221_Wtaunu.root")
  path_outpdf = "mc15_completeWtaunu_221_TauProm.pdf"
elif args.input == 5:
  print "Using SHERPA Trigger TEST"
  path_orig = "/home/a/A.Mann/Daten/own/TauProm_201612/user.mann.0001.v027-8.SUSYTauAnalysisCycle.mc15_363331.e4709_s2726_r7725_r7676_p2719_SUSYTauAnalysis.root/user.mann.10121791._000001.SUSYTauAnalysis.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.Trigger.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.Trigger.root")
  path_outpdf = "TauPromotionPlots_Sherpa_Trigger.pdf"
  
elif args.input == 6:
  print "Using SHERPA NEW"
  path_orig = "/home/a/Adam.Samara/mc15_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.merge.DAOD_SUSY3.e5340_s2726_r7772_r7676_p2949_tid10307818_00/DAOD_SUSY3.10307818._000044.pool.root.1"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.mc15_13TeV.364186_DAOD_SUSY3.10307818._000044.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.mc15_13TeV.364186_DAOD_SUSY3.10307818._000044.root")
  path_outpdf = "TauPromotionPlots_Sherpa_mc15_13TeV.364186_DAOD_SUSY3.10307818._000044.pdf"
  

else:
  print "Unknown input set:", args.input  
  sys.exit(1)


### helpers

def PlotNtaus():
  
  c2 = ROOT.TCanvas("c2")
  c2.SetLogy()

  same = "PE"
  for idx, nx in enumerate(["n_cont_taus", "n_true_taus", "n_medium_taus", "n_quality_taus_measure", "n_quality_taus_promote", "n_quality_nonreal_taus_measure", "n_quality_nonreal_taus_promote"]):
  
    hname = "hist" + nx
    tree_orig.Draw("%s >>%s(10, -.5, 9.5)" % (nx, hname), "event_weight", same)
    if not hasattr(ROOT, hname):
      print hname, "??"
      continue
    hist = getattr(ROOT, hname)
    hist.SetTitle(nx)
    hist.GetXaxis().SetTitle("n_{#tau}")
    hist.SetLineColor(idx + 1)
    same = "PESAME"
  
  c2.BuildLegend()
  AlexROOT.Tag(path_orig)
  c2.Print(path_outpdf)




def PlotTauPtTest():
  
  "most important plot: compare spectrum of MET before and after promotion"
  
  c2 = ROOT.TCanvas("c2")
  c2.SetLogy()
  
  tree_orig.Draw("tau_pt >>histptp(%d, 0, 2e5)" % (bins_pt), "event_weight                  * (is_quality_promote && (n_quality_taus_promote >= 2))", "PE")
  ROOT.histptp.GetXaxis().SetTitle("tau p_{T} [MeV]")
  ROOT.histptp.SetTitle("promote")

  tree_orig.Draw("tau_pt >>histptn(%d, 0, 2e5)" % (bins_pt), "event_weight                  * (is_quality_measure && (n_quality_taus >= 2))", "PESAME")
  ROOT.histptn.SetLineColor(7)
  ROOT.histptn.SetTitle("nominal")

  tree_orig.Draw("tau_pt >>histptr(%d, 0, 2e5)" % (bins_pt), "event_weight * promote_weight * (is_quality_promote && (n_quality_taus_promote >= 2))", "PESAME")
  ROOT.histptr.SetLineColor(2)
  ROOT.histptr.SetTitle("reweighted")
  
  c2.BuildLegend()
  AlexROOT.Tag(path_orig)
  c2.Print(path_outpdf + "(") # first plot!
  

  
  # once again with ratio plot
  rp = RatioPlot.RatioPlot()  
  rp.ratio_max = 2
  rp.AddDown(ROOT.histptn, ROOT.histptn.GetTitle())
  rp.AddUp  (ROOT.histptp, ROOT.histptp.GetTitle())
  rp.AddUp  (ROOT.histptr, ROOT.histptr.GetTitle())
  rp.DrawOverlayAndRatio()
  rp.DrawLegend()
  AlexROOT.Tag(path_orig)
  rp.GetCanvas().Print(path_outpdf)

  c2.Close()
  rp.GetCanvas().Close() # avoid ROOT crash
  
def PlotNthTauPtTest():

  "most important plot: plot nth leading tau pt"
  
  for idx in [0, 1]:
    
    c2 = ROOT.TCanvas("c_nthtau_%d" % idx)
    c2.SetLogy()
    #import q
    name_n = "histpt%dn" % idx
    name_p = "histpt%dp" % idx
  
    tree_orig.Draw("quality_tau%d_pt_measure >>%s(%d, 0, 2e5)" % (idx, name_p, bins_pt), "event_weight                  * (n_quality_taus >= 2)", "PE")
    hist_p = getattr(ROOT, name_p)
    hist_p .SetLineColor(7)
    hist_p .GetXaxis().SetTitle("p_{T}(#tau_{%d}) [MeV]" % idx)
    hist_p .SetTitle("nominal")

    tree_orig.Draw("quality_tau%d_pt_promote >>%s(%d, 0, 2e5)" % (idx, name_n, bins_pt), "event_weight * promote_weight * (n_quality_taus_promote >= 2)", "PESAME")
    hist_n = getattr(ROOT, name_n)
    hist_n .SetLineColor(2)
    hist_n .SetTitle("reweighted")
    
    c2.BuildLegend()
    AlexROOT.Tag(path_orig)
    c2.Print(path_outpdf + "(") # first plot!
    
    # once again with ratio plot
    rp = RatioPlot.RatioPlot()  
    rp.ratio_max = 2
    rp.AddDown(hist_n, hist_n.GetTitle())
    rp.AddUp  (hist_p, hist_p.GetTitle())
    rp.DrawOverlayAndRatio()
    rp.DrawLegend()
    AlexROOT.Tag(path_orig)
    rp.GetCanvas().Print(path_outpdf)

    c2.Delete()


def PlotNumbersPerBin():
  
  c1 = ROOT.TCanvas("c1")
  c1.Divide(2, 2)
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetPaintTextFormat(".1f")

  # can adapt selection here if promotion done on different set than promotion
  selection = "is_selected_promote"

  c1.cd(1).SetLogz()
  tree_meas .Draw("n_quality_taus_measure:n_cont_taus >>histnn(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnn.SetMarkerSize(2)
  c1.cd(2).SetLogz()
  tree_prom.Draw("n_quality_taus_measure:n_cont_taus >>histnp(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * promote_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnp.SetMarkerSize(2)
  c1.cd(3).SetLogz()
  tree_meas .Draw("n_quality_nonreal_taus:n_cont_nonreal_taus >>histnn2(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnn2.SetMarkerSize(2)
  AlexROOT.Tag(path_inf_meas)
  c1.cd(4).SetLogz()
  tree_prom.Draw("n_quality_nonreal_taus:n_cont_nonreal_taus >>histnp2(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * promote_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnp2.SetMarkerSize(2)
  AlexROOT.Tag(path_inf_prom)

  #AlexROOT.StdSave(c1, "tau_promotion_compare_%s_n" % version)
  #AlexROOT.StdSave(c1, "TauPromotionPlots.pdf)")
  c1.Print(path_outpdf)





def PlotEfficiency():

  ### plot efficiencies
  for name in ["h_eff_1prong", "h_eff_3prong"]:
    hname = "Efficiency/" + name
    h_eff = inf_meas.GetKey(hname)
    #if not h_eff:
      #Alex.PrintError("Couldn't find %s" % hname)
      #continue
    c4 = ROOT.TCanvas("c4")
    h_eff = inf_meas.Get(hname)
    h_eff.SetTitle(name)
    h_eff.SetMinimum(0.0)
    h_eff.SetMaximum(0.075)
    h_eff.GetXaxis().SetRangeUser(0, 300000)
    h_eff.Draw("COLZTEXT90E")
    AlexROOT.Tag(path_inf_meas)
    c4.Print(path_outpdf)
    c4.SetLogx(1)
    c4.Print(path_outpdf)
    c4.SetLogx(0)
    c4.SetTitle("Efficiency")
  


def PlotTauPtforIDs():
  
  ### plot tau pt spectrum for all tau IDs and fake and true taus separately
  
  c1 = ROOT.TCanvas("c1")
  c1.SetLogy()
  
  classes = {
    "all" : "1",
    "true": "tau_classification == 0",
    "fake": "tau_classification != 0",
  }
  tauids = ["none", "loose", "medium", "tight"]
  for classname, classcondition in classes.items():
    
    h_tau_pt = {}
    for i, tau_id in enumerate(tauids):
      hname = "h_taupt_" + tau_id + classname
      tree_orig.Draw("tau_pt / 1000. >>%s(50, 0, 200)" % hname, "(%s) * (tau_quality >= %d) * evt_weight_nom" % (classcondition, i), "COLZTEXT45")

    same = ""
    for i, tau_id in enumerate(tauids):
      hname = "h_taupt_" + tau_id + classname
      h = getattr(ROOT, hname)
      h.SetTitle("%s, %s taus" % (tau_id, classname))
      h.GetXaxis().SetTitle("tau p_{T} [GeV]")
      h.SetLineColor(i + 1)
      h.SetMarkerColor(i + 1)
      #h.Draw(same)
      #same = "SAME"
    
    
    # make ratio plot
    rp = RatioPlot.RatioPlot()
    for i, tau_id in enumerate(tauids):
      hname = "h_taupt_" + tau_id + classname
      h = getattr(ROOT, hname)
      if i == 0:
        rp.AddDown(h, h.GetTitle())
      else:
        rp.AddUp  (h, h.GetTitle())
        
    rp.DrawOverlayAndRatio()
    rp.DrawLegend()
    AlexROOT.Tag(path_orig)
    rp.GetCanvas().Print(path_outpdf)
    
  
def PlotEventWeights():

  c1 = ROOT.TCanvas("c1")
  c1.SetLogy()
  
  tree_orig.Draw("event_weight >>hev_prom(50, -5, 5)", "(n_quality_taus_promote >= 2)", "") # more entries
  ROOT.hev_prom.GetXaxis().SetTitle("event weight")
  tree_orig.Draw("event_weight >>hev_meas(50, -5, 5)", "(n_quality_taus >= 2)", "SAME")
  ROOT.hev_meas.SetLineColor(2)
  ROOT.hev_meas.SetLineStyle(2)
  
  c1.Print(path_outpdf)



### main
#import q
#q.q(123)

# read input

                               
Alex.PrintBoldFmtA("Reading input from:\n  Orig: *%s* \n  Measure: *%s*\n  Promote: *%s*", path_origs, path_inf_meas, path_inf_prom) 
#inf_orig = ROOT.TFile(path_orig)
inf_orig = ROOT.TChain("CENTRAL_Loose")
for path_orig in path_origs:
  inf_orig.Add(path_orig)

inf_meas = ROOT.TFile(path_inf_meas)
inf_prom = ROOT.TFile(path_inf_prom)
#tree_orig = inf_orig.Get("CENTRAL_Loose")
tree_orig = inf_orig

treename = "susyout"
tree_meas  = inf_meas.Get(treename)
if not tree_meas:
  treename = "CENTRAL_Loose"
  tree_meas  = inf_meas.Get(treename)
tree_prom = inf_prom.Get(treename)
Alex.PrintBoldFmtA("Tree name is %s", treename)

# make friends
tree_orig.AddFriend(treename, path_inf_meas)
tree_orig.AddFriend(treename, path_inf_prom)




### create plots
AlexROOT.SetBatchMode(True)
ROOT.gStyle.SetOptTitle(1)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPaintTextFormat(".3f")


############# call plot functions from above

PlotTauPtTest()
PlotNtaus()
PlotNthTauPtTest()

### exit strategy...
#c2 = ROOT.TCanvas("c1")
#c2.Print(path_outpdf + ")")
#sys.exit(1)

PlotEfficiency()

if path_orig:
  PlotTauPtforIDs()

#PlotNumbersPerBin()

#ROOT.gStyle.SetOptTitle(1)
ROOT.gStyle.SetOptStat(1)
PlotEventWeights()


print 1
############# plot weights
c2 = ROOT.TCanvas("c1")
c2.SetLogx()
tree_prom.Draw("promote_weight", "", "")
c2.Print(path_outpdf)
tree_orig.Draw("promote_weight", "(n_quality_taus_promote >= 2)", "")
c2.Print(path_outpdf)
c2.SetLogx(0)

tree_prom.Draw("promote_eff", "", "")
c2.Print(path_outpdf)
tree_orig.Draw("promote_eff", "(n_quality_taus_promote >= 2)", "")
c2.Print(path_outpdf)
print 2



############# plot PhDs
c2.SetLogy(0)
c2.SetLogz()
tree_orig.Draw("idx_phd:nPhD >>histphds(8, -.5, 7.5, 5, -.5, 4.5", "event_weight", "COLZTEXT45")
ROOT.histphds.GetXaxis().SetTitle("nPhD")
ROOT.histphds.GetYaxis().SetTitle("idx_phd")
c2.Print(path_outpdf)
  
print 3
############ plot MET

c2.SetLogy()

# plottet dasselbe, nur mit einem (gleichen) Eintrag für jedes Tau in einem Ereignis
tree_meas.Draw("evt_MET >>histmetn(30, 0, 1e5)", "event_weight *                  (is_quality_measure && is_selected_measure && (n_quality_taus >= 2))", "PE")
ROOT.histmetn.GetXaxis().SetTitle("MET [MeV]")
ROOT.histmetn.SetLineColor(2)
tree_prom.Draw("evt_MET >>histmetp            ", "event_weight * promote_weight * (is_quality_promote && is_selected_promote && (n_quality_taus_promote >= 2))", "PESAME")
c2.Print(path_outpdf)  
tree_orig.Draw("evt_MET >>histmetn2(40, 0, 2e5)", "event_weight *                  (                                            (n_quality_taus >= 2))", "PE")
ROOT.histmetn2.SetTitle("nominal")
ROOT.histmetn2.GetXaxis().SetTitle("MET [MeV]")
ROOT.histmetn2.SetLineColor(2)
tree_orig.Draw("evt_MET >>histmetp2            ", "event_weight * promote_weight * (                                            (n_quality_taus_promote >= 2))", "PESAME")
ROOT.histmetp2.SetTitle("reweighted")
ROOT.histmetp2.GetXaxis().SetTitle("MET [MeV]")
c2.BuildLegend()
c2.Print(path_outpdf)
# make ratio
rp = RatioPlot.RatioPlot()
rp.ratio_max = 2
rp.AddDown(ROOT.histmetn2, ROOT.histmetn2.GetTitle())
rp.AddUp  (ROOT.histmetp2, ROOT.histmetp2.GetTitle())  
rp.DrawOverlayAndRatio()
rp.DrawLegend()
rp.GetCanvas().Print(path_outpdf)


############ is flags
c2.cd()
c2.SetLogy(0)

tree_orig.Draw("(is_quality + 2*is_selected_measure + 4*is_quality_measure) >>histisn(8, -.5, 7.5)", "event_weight", "HIST")
ROOT.histisn.SetLineColor(2)
tree_orig.Draw("(is_quality + 2*is_selected_promote + 4*is_quality_promote) >>histisp(8, -.5, 7.5)", "event_weight", "HIST SAME")
ROOT.histisp.SetLineStyle(2)

c2.Print(path_outpdf + ")")

=======
# -*- coding: utf-8 -*-
#
# Tau promotion light.
#
# Previous version: compare.py
#
# (85)
#
# $Id: TauPromotionPlots.py 2687 2016-12-21 16:50:11Z eis $


### imports 
import os
import sys
import Alex
import ROOT
import argparse
import AlexROOT
import RatioPlot


### config + cmdline
version = "default"
bins_pt = 100

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', type = int, default = 2)
args = parser.parse_args()

path = "/project/etp/Samara/SFrameTest/SFrame/Promote/run"
if args.input == 0:
  print "Using DUMMY TREE"
  path_orig = "/home/a/Adam.Samara/DummyTree/Wdummy.root"
  path_inf_meas = os.path.join(path, "Measure.Dummy.v2.root")
  path_inf_prom = os.path.join(path, "Promote.Dummy.v2.root")
  path_outpdf = "TauPromotionPlots_Dummy2.pdf"

elif args.input == 1:
  print "Using Alpgen? TEST"
  path_orig = ""
  path_inf_meas = os.path.join(path, "Measure.MC_OldMGP8_met.v1.root")
  path_inf_prom = os.path.join(path, "Promote.MC_OldMGP8_met.v1.root")
  path_outpdf = "TauPromotionPlots_Alpgen.pdf"
elif args.input == 2:
  print "Using SHERPA TEST"
  path_orig = "/home/a/A.Mann/TauAnalysisCode/1_xAOD/atlas-phys-susy-tau-framework-analysis__NEW/xAODSUSYTauAnalysis/run/SUSYTauAnalysisCycle.364184_Sherpa_Wtaunu.SUSY3.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.v1.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.v1.root")
  path_outpdf = "TauPromotionPlots_Sherpa.pdf"
elif args.input == 3:
  print "Using SHERPA xAOD TEST"
  path_orig = "/home/a/A.Mann/TauAnalysisCode/1_xAOD/atlas-phys-susy-tau-framework-analysis__NEW/xAODSUSYTauAnalysis/run/SUSYTauAnalysisCycle.364184_Sherpa_Wtaunu.xAOD.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.xAOD.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.xAOD.root")
  path_outpdf = "TauPromotionPlots_Sherpa_xAOD.pdf"
elif args.input == 4:
  print "Using SHERPA unbias TEST"
  #path_orig = "/home/a/A.Mann/TauAnalysisCode/1_xAOD/atlas-phys-susy-tau-framework-analysis__NEW/xAODSUSYTauAnalysis/run/SUSYTauAnalysisCycle.364184_Sherpa_Wtaunu.xAOD.root"
  path_orig = "/home/a/A.Mann/Daten/own/TauProm_201612/user.mann.0001.v027-8.SUSYTauAnalysisCycle.mc15_363331.e4709_s2726_r7725_r7676_p2719_SUSYTauAnalysis.root/user.mann.10121791._000001.SUSYTauAnalysis.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.unbias.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.unbias.root")
  path_outpdf = "TauPromotionPlots_Sherpa_unbias.pdf"
elif args.input == 5:
  print "Using SHERPA Trigger TEST"
  path_orig = "/home/a/A.Mann/Daten/own/TauProm_201612/user.mann.0001.v027-8.SUSYTauAnalysisCycle.mc15_363331.e4709_s2726_r7725_r7676_p2719_SUSYTauAnalysis.root/user.mann.10121791._000001.SUSYTauAnalysis.root"
  path_inf_meas = os.path.join(path, "Measure.MC_Sherpa_met.Trigger.root")
  path_inf_prom = os.path.join(path, "Promote.MC_Sherpa_met.Trigger.root")
  path_outpdf = "TauPromotionPlots_Sherpa_Trigger.pdf"

else:
  print "Unknown input set:", args.input  
  sys.exit(1)


### helpers

def PlotNtaus():
  
  c2 = ROOT.TCanvas("c2")
  c2.SetLogy()
  
  same = "PE"
  for idx, nx in enumerate(["n_cont_taus", "n_true_taus", "n_medium_taus", "n_medium_taus_measure", "n_medium_taus_promote", "n_medium_nonreal_taus_measure", "n_medium_nonreal_taus_promote"]):
  
    hname = "hist" + nx
    tree_orig.Draw("%s >>%s(10, -.5, 9.5)" % (nx, hname), "event_weight", same)
    if not hasattr(ROOT, hname):
      print hname, "??"
      continue
    hist = getattr(ROOT, hname)
    hist.GetXaxis().SetTitle("n_{#tau}")
    hist.SetTitle(nx)
    hist.SetLineColor(idx + 1)
    same = "PESAME"

  c2.BuildLegend()
  AlexROOT.Tag(path_orig)
  c2.Print(path_outpdf)




def PlotTauPtTest():
  
  "most important plot: compare spectrum of MET before and after promotion"
  
  c2 = ROOT.TCanvas("c2")
  c2.SetLogy()
  
  tree_orig.Draw("tau_pt >>histptp(%d, 0, 2e5)" % (bins_pt), "event_weight                  * (is_medium_promote && (n_medium_taus_promote >= 2))", "PE")
  ROOT.histptp.GetXaxis().SetTitle("tau p_{T} [MeV]")
  ROOT.histptp.SetTitle("promote")

  tree_orig.Draw("tau_pt >>histptn(%d, 0, 2e5)" % (bins_pt), "event_weight                  * (is_medium_measure && (n_medium_taus_measure >= 2))", "PESAME")
  ROOT.histptn.SetLineColor(7)
  ROOT.histptn.SetTitle("nominal")

  tree_orig.Draw("tau_pt >>histptr(%d, 0, 2e5)" % (bins_pt), "event_weight * promote_weight * (is_medium_promote && (n_medium_taus_promote >= 2))", "PESAME")
  ROOT.histptr.SetLineColor(2)
  ROOT.histptr.SetTitle("reweighted")
  
  c2.BuildLegend()
  AlexROOT.Tag(path_orig)
  c2.Print(path_outpdf + "(") # first plot!
  

  
  # once again with ratio plot
  rp = RatioPlot.RatioPlot()  
  rp.ratio_max = 2
  rp.AddDown(ROOT.histptn, ROOT.histptn.GetTitle())
  rp.AddUp  (ROOT.histptp, ROOT.histptp.GetTitle())
  rp.AddUp  (ROOT.histptr, ROOT.histptr.GetTitle())
  rp.DrawOverlayAndRatio()
  rp.DrawLegend()
  AlexROOT.Tag(path_orig)
  rp.GetCanvas().Print(path_outpdf)

  c2.Close()
  rp.GetCanvas().Close() # avoid ROOT crash

def PlotNthTauPtTest():

  "most important plot: plot nth leading tau pt"
  
  for idx in [0, 1]:
    
    c2 = ROOT.TCanvas("c_nthtau_%d" % idx)
    c2.SetLogy()
    #import q
    name_n = "histpt%dn" % idx
    name_p = "histpt%dp" % idx
  
    tree_orig.Draw("medium_tau%d_pt_measure >>%s(%d, 0, 2e5)" % (idx, name_p, bins_pt), "event_weight                  * (n_medium_taus >= 2)", "PE")
    hist_p = getattr(ROOT, name_p)
    hist_p .SetLineColor(7)
    hist_p .GetXaxis().SetTitle("p_{T}(#tau_{%d}) [MeV]" % idx)
    hist_p .SetTitle("nominal")

    tree_orig.Draw("medium_tau%d_pt_promote >>%s(%d, 0, 2e5)" % (idx, name_n, bins_pt), "event_weight * promote_weight * (n_medium_taus_promote >= 2)", "PESAME")
    hist_n = getattr(ROOT, name_n)
    hist_n .SetLineColor(2)
    hist_n .SetTitle("reweighted")
    
    c2.BuildLegend()
    AlexROOT.Tag(path_orig)
    c2.Print(path_outpdf + "(") # first plot!
    
    # once again with ratio plot
    rp = RatioPlot.RatioPlot()  
    rp.ratio_max = 2
    rp.AddDown(hist_n, hist_n.GetTitle())
    rp.AddUp  (hist_p, hist_p.GetTitle())
    rp.DrawOverlayAndRatio()
    rp.DrawLegend()
    AlexROOT.Tag(path_orig)
    #rp.GetCanvas().Print(path_outpdf)

    c2.Delete()


def PlotNumbersPerBin():
  
  c1 = ROOT.TCanvas("c1")
  c1.Divide(2, 2)
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetPaintTextFormat(".1f")

  # can adapt selection here if promotion done on different set than promotion
  selection = "is_selected_promote"

  c1.cd(1).SetLogz()
  tree_meas .Draw("n_medium_taus:n_cont_taus >>histnn(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnn.SetMarkerSize(2)
  c1.cd(2).SetLogz()
  tree_prom.Draw("n_medium_taus:n_cont_taus >>histnp(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * promote_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnp.SetMarkerSize(2)
  c1.cd(3).SetLogz()
  tree_meas .Draw("n_medium_nonreal_taus:n_cont_nonreal_taus >>histnn2(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnn2.SetMarkerSize(2)
  AlexROOT.Tag(path_inf_meas)
  c1.cd(4).SetLogz()
  tree_prom.Draw("n_medium_nonreal_taus:n_cont_nonreal_taus >>histnp2(8, -.5, 7.5, 5, -.5, 4.5)", "event_weight * promote_weight * (%s)" % selection, "COLZTEXT45")
  ROOT.histnp2.SetMarkerSize(2)
  AlexROOT.Tag(path_inf_prom)

  #AlexROOT.StdSave(c1, "tau_promotion_compare_%s_n" % version)
  #AlexROOT.StdSave(c1, "TauPromotionPlots.pdf)")
  c1.Print(path_outpdf)





def PlotEfficiency():

  ### plot efficiencies
  for name in ["h_eff_1prong", "h_eff_3prong"]:
    hname = "Efficiency/" + name
    h_eff = inf_meas.GetKey(hname)
    #if not h_eff:
      #Alex.PrintError("Couldn't find %s" % hname)
      #continue
    c4 = ROOT.TCanvas("c4")
    h_eff = inf_meas.Get(hname)
    h_eff.SetTitle(name)
    h_eff.SetMinimum(0.0)
    h_eff.SetMaximum(0.075)
    h_eff.GetXaxis().SetRangeUser(0, 300000)
    h_eff.Draw("COLZTEXT90E")
    AlexROOT.Tag(path_inf_meas)
    c4.Print(path_outpdf)
    c4.SetLogx(1)
    c4.Print(path_outpdf)
    c4.SetLogx(0)
  


def PlotTauPtforIDs():
  
  ### plot tau pt spectrum for all tau IDs and fake and true taus separately
  
  c1 = ROOT.TCanvas("c1")
  c1.SetLogy()
  
  classes = {
    "all" : "1",
    "true": "tau_classification == 0",
    "fake": "tau_classification != 0",
  }
  tauids = ["none", "loose", "medium", "tight"]
  for classname, classcondition in classes.items():
    
    h_tau_pt = {}
    for i, tau_id in enumerate(tauids):
      hname = "h_taupt_" + tau_id + classname
      tree_orig.Draw("tau_pt / 1000. >>%s(50, 0, 200)" % hname, "(%s) * (tau_quality >= %d) * evt_weight_nom" % (classcondition, i), "COLZTEXT45")

    same = ""
    for i, tau_id in enumerate(tauids):
      hname = "h_taupt_" + tau_id + classname
      h = getattr(ROOT, hname)
      h.SetTitle("%s, %s taus" % (tau_id, classname))
      h.GetXaxis().SetTitle("tau p_{T} [GeV]")
      h.SetLineColor(i + 1)
      h.SetMarkerColor(i + 1)
      #h.Draw(same)
      #same = "SAME"
    
    
    # make ratio plot
    rp = RatioPlot.RatioPlot()
    for i, tau_id in enumerate(tauids):
      hname = "h_taupt_" + tau_id + classname
      h = getattr(ROOT, hname)
      if i == 0:
        rp.AddDown(h, h.GetTitle())
      else:
        rp.AddUp  (h, h.GetTitle())
        
    rp.DrawOverlayAndRatio()
    rp.DrawLegend()
    AlexROOT.Tag(path_orig)
    rp.GetCanvas().Print(path_outpdf)
    
  
def PlotEventWeights():

  c1 = ROOT.TCanvas("c1")
  c1.SetLogy()
  
  tree_orig.Draw("event_weight >>hev_prom(50, -5, 5)", "(n_medium_taus_promote >= 2)", "") # more entries
  ROOT.hev_prom.GetXaxis().SetTitle("event weight")
  tree_orig.Draw("event_weight >>hev_meas(50, -5, 5)", "(n_medium_taus >= 2)", "SAME")
  ROOT.hev_meas.SetLineColor(2)
  ROOT.hev_meas.SetLineStyle(2)
  
  c1.Print(path_outpdf)



### main
#import q
#q.q(123)

# read input

                               
Alex.PrintBoldFmtA("Reading input from:\n  Orig: *%s* \n  Measure: *%s*\n  Promote: *%s*", path_orig, path_inf_meas, path_inf_prom) 
inf_orig = ROOT.TFile(path_orig)
inf_meas = ROOT.TFile(path_inf_meas)
inf_prom = ROOT.TFile(path_inf_prom)
tree_orig = inf_orig.Get("CENTRAL_Loose")

treename = "susyout"
tree_meas  = inf_meas.Get(treename)
if not tree_meas:
  treename = "CENTRAL_Loose"
  tree_meas  = inf_meas.Get(treename)
tree_prom = inf_prom.Get(treename)
Alex.PrintBoldFmtA("Tree name is %s", treename)

# make friends
tree_orig.AddFriend(treename, path_inf_meas)
tree_orig.AddFriend(treename, path_inf_prom)




### create plots
AlexROOT.SetBatchMode(True)
ROOT.gStyle.SetOptTitle(1)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetPaintTextFormat(".3f")


############# call plot functions from above

PlotTauPtTest()
PlotNtaus()
#PlotNthTauPtTest()

### exit strategy...
#c2 = ROOT.TCanvas("c1")
#c2.Print(path_outpdf + ")")
#sys.exit(1)

PlotEfficiency()

if path_orig:
  PlotTauPtforIDs()

#PlotNumbersPerBin()

ROOT.gStyle.SetOptTitle(1)
ROOT.gStyle.SetOptStat(1)
PlotEventWeights()


print 1
############# plot weights
c2 = ROOT.TCanvas("c1")
c2.SetLogx()
tree_prom.Draw("promote_weight", "", "")
c2.Print(path_outpdf)
tree_orig.Draw("promote_weight", "(n_medium_taus_promote >= 2)", "")
c2.Print(path_outpdf)
c2.SetLogx(0)

tree_prom.Draw("promote_eff", "", "")
c2.Print(path_outpdf)
tree_orig.Draw("promote_eff", "(n_medium_taus_promote >= 2)", "")
c2.Print(path_outpdf)
print 2



############# plot PhDs
c2.SetLogy(0)
c2.SetLogz()
tree_orig.Draw("idx_phd:nPhD >>histphds(8, -.5, 7.5, 5, -.5, 4.5", "event_weight", "COLZTEXT45")
ROOT.histphds.GetXaxis().SetTitle("nPhD")
ROOT.histphds.GetYaxis().SetTitle("idx_phd")
c2.Print(path_outpdf)
  
print 3
############ plot MET

c2.SetLogy()
'''
# plottet dasselbe, nur mit einem (gleichen) Eintrag für jedes Tau in einem Ereignis
tree_meas.Draw("evt_MET >>histmetn(30, 0, 1e5)", "event_weight *                  (is_medium_measure && is_selected_promote && (n_medium_taus >= 2))", "PE")
ROOT.histmetn.GetXaxis().SetTitle("MET [MeV]")
ROOT.histmetn.SetLineColor(2)
tree_prom.Draw("evt_MET >>histmetp            ", "event_weight * promote_weight * (is_medium_promote && is_selected_promote && (n_medium_taus >= 2))", "PESAME")
c2.Print(path_outpdf)  
tree_orig.Draw("evt_MET >>histmetn2(40, 0, 2e5)", "event_weight *                  (                                            (n_medium_taus >= 2))", "PE")
ROOT.histmetn2.SetTitle("nominal")
ROOT.histmetn2.GetXaxis().SetTitle("MET [MeV]")
ROOT.histmetn2.SetLineColor(2)
tree_orig.Draw("evt_MET >>histmetp2            ", "event_weight * promote_weight * (                                            (n_medium_taus_promote >= 2))", "PESAME")
ROOT.histmetp2.SetTitle("reweighted")
ROOT.histmetp2.GetXaxis().SetTitle("MET [MeV]")
c2.BuildLegend()
c2.Print(path_outpdf)
# make ratio
rp = RatioPlot.RatioPlot()
rp.ratio_max = 2
rp.AddDown(ROOT.histmetn2, ROOT.histmetn2.GetTitle())
rp.AddUp  (ROOT.histmetp2, ROOT.histmetp2.GetTitle())  
rp.DrawOverlayAndRatio()
rp.DrawLegend()
rp.GetCanvas().Print(path_outpdf)
'''

############ is flags
c2.cd()
c2.SetLogy(0)

tree_orig.Draw("(is_medium + 2*is_selected_measure + 4*is_medium_measure) >>histisn(8, -.5, 7.5)", "event_weight", "HIST")
ROOT.histisn.SetLineColor(2)
tree_orig.Draw("(is_medium + 2*is_selected_promote + 4*is_medium_promote) >>histisp(8, -.5, 7.5)", "event_weight", "HIST SAME")
ROOT.histisp.SetLineStyle(2)

c2.Print(path_outpdf + ")")

>>>>>>> 7ca2c2b04598a35c348470f3888c5c3412d02920
