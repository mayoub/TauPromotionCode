// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $

// Local include(s):
#include "../include/TruthMatch.h"

ClassImp( TruthMatch );

TruthMatch::TruthMatch()
  : SCycleBase() {

  DeclareProperty( "RecoTreeName", m_c_recoTreeName );
  DeclareProperty( "MetaTreeName", m_c_metaTreeName );

  SetLogName( GetName() );
}

TruthMatch::~TruthMatch() {

}

void TruthMatch::BeginCycle() throw( SError ) {

  return;

}

void TruthMatch::EndCycle() throw( SError ) {

  return;

}

void TruthMatch::BeginInputData( const SInputData& ) throw( SError ) {

  m_logger << ::INFO << "Setting up histograms..." << SLogger::endmsg;
  InitializeHistograms();
  return;

}

void TruthMatch::InitializeHistograms() {
  
//   h_eff_u = Book( Teff("h_eff_u", "h_eff_u", 9, 2e4, 2e5, nbinsy, ymin, ymax), "Efficiency" );
  
}

void TruthMatch::EndInputData( const SInputData& ) throw( SError ) {

  return;

}

void TruthMatch::BeginInputFile( const SInputData& ) throw( SError ) {

  //
  // Connect the input variables:
  //
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_pt"                   , m_tau_pt                   );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_prongness"            , m_tau_prongness            );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_phi"                  , m_tau_phi                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_eta"                  , m_tau_eta                  );
//   ConnectVariable( m_recoTreeName.c_str(), "is_container_20"          , m_is_container_20          ); // FIXME: add back 
  ConnectVariable( m_c_recoTreeName.c_str(), "is_container"             , m_is_container             );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_loose"                 , m_is_loose                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_medium"                , m_is_medium                );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_true"                  , m_is_true                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "faketype"                 , m_faketype                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "faketype_full"            , m_faketype_full            );
//   ConnectVariable( m_recoTreeName.c_str(), "n_cont_taus_20"           , m_n_cont_taus_20           );
  ConnectVariable( m_c_recoTreeName.c_str(), "n_true_taus"              , m_n_true_taus              );
  ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_taus"            , m_n_medium_taus            );
  ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_nonreal_taus"      , m_n_cont_nonreal_taus      );
  ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_nonreal_taus"    , m_n_medium_nonreal_taus    );
  ConnectVariable( m_c_recoTreeName.c_str(), "met_mod"                  , m_met_mod                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_complete_bline", m_evt_weight_complete_bline);
//   ConnectVariable( m_recoTreeName.c_str(), "evt_MT2"                  , m_evt_MT2                  );
//   ConnectVariable( m_recoTreeName.c_str(), "evt_M_eff"                , m_evt_M_eff                );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_xSec"          , m_evt_weight_xSec          );
  ConnectVariable( m_c_recoTreeName.c_str(), "event_weight"             , m_event_weight             );
//   ConnectVariable( m_recoTreeName.c_str(), "met_phi"                  , m_met_phi                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "mcid"                     , m_mcid                     );
  
  return;

}

void TruthMatch::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {

  return;

}

