// $Id: Promote.cxx 2686 2016-12-18 21:21:22Z eis $

// Local include(s):
#include "../include/Promote.h"
#include <cmath>
#include <iostream>
#include <algorithm>

std::string Promote::m_c_recoTreeName;
std::string Promote::m_c_metaTreeName;
std::string Promote::m_c_secondaxis;
double Promote::m_c_bin_maxmet;
double Promote::m_c_mintaupt;
double Promote::m_c_meanfakeeff_tauptcutoff;
double Promote::m_c_meanfakeeff_metcutoff;
std::string Promote::m_c_efferror;
bool Promote::m_c_isenu;
bool Promote::m_c_ismunu;
bool Promote::m_c_istaunu;
bool Promote::m_c_ismatched;
bool Promote::m_c_ismatched_sign;
bool Promote::m_c_isunmatched;
std::string Promote::m_c_requiretrigger;
std::string Promote::m_c_pathhisteff;

double comparison_weight_1prong = 0.;
double comparison_weight_3prong = 0.;
double comparison_eff_1prong = 0.;
double comparison_eff_3prong = 0.;

int Promote::m_c_selecttautype; 


ClassImp( Promote );

Promote::Promote():
  SCycleBase() {

  DeclareProperty( "RecoTreeName", m_c_recoTreeName );
  DeclareProperty( "MetaTreeName", m_c_metaTreeName );

  DeclareProperty( "SecondAxis"  , m_c_secondaxis );
  DeclareProperty( "MaxMET"      , m_c_bin_maxmet = 500000. );
  DeclareProperty( "MinTauPt"    , m_c_mintaupt = 2e4 );
  DeclareProperty( "TauPtCutOff" , m_c_meanfakeeff_tauptcutoff = 12e12);  // use as upper limit, above which a manually implemented fit value is used; to disable, set to very high value 	//TODO: improve disable mechanism
  DeclareProperty( "METCutOff"   , m_c_meanfakeeff_metcutoff = 0);	 // same for met; if only pt cut shall be used, set to zero
  DeclareProperty( "EffError"    , m_c_efferror = "");		// "Max" for Content + Error; "Min" for Content - Error; "" for Content as return value in GetEfficiency()
  DeclareProperty( "Ise"         , m_c_isenu = false);				// Sets additional selection for e samples, primarily used for matching
  DeclareProperty( "Ismu"        , m_c_ismunu = false);				// same for muons
  DeclareProperty( "Istau"       , m_c_istaunu = true);			// disables matching and preselection for e and mu
  DeclareProperty( "IsMatched"   , m_c_ismatched = true);			// switch between matching (true, only cont taus matched with e/mu are used for eff and prom) and antimatching (false)
  DeclareProperty( "IsMatchedSign",m_c_ismatched_sign = true);			// swaps matched and antimatched
  DeclareProperty( "IsUnmatched" , m_c_isunmatched = true);			// disables matched/antimatched
  DeclareProperty( "RequireTrigger", m_c_requiretrigger = "");
  
  DeclareProperty( "PathHistEff" , m_c_pathhisteff = "");

  DeclareProperty( "SelectTauType" , m_c_selecttautype = 2);

  SetLogName( GetName() );
  // m_config.SetPrintEveryEvents(50000);
  
  higheffcounter = 0;
  loweffcounter  = 0;
  negeffcounter  = 0;
  oneeffcounter  = 0;
  generalcounter = 0;
  m_n_selected_events = 0;
  
}

bool Promote::Is_tau_quality(int tau_index){
//   if (((*m_tau_quality)[tau_index] && m_is_loose) >= m_c_selecttautype){  // use condition m_is_loose to make sure baseline is added
  if (((*m_tau_quality)[tau_index] >= m_c_selecttautype) && (*m_is_loose)[tau_index]){
    return true;
  } else {
    return false;
  }
    
}

Promote::~Promote() {

}

void Promote::BeginCycle() throw( SError ) {

  ParseConfiguration();
  
  m_logger << INFO << "Minimum tau pt: " << m_c_mintaupt << SLogger::endmsg;
  m_logger << INFO << "m_secondaxis  : " << m_secondaxis << SLogger::endmsg;
  m_logger << INFO << "SelectTauType: " << m_c_selecttautype << SLogger::endmsg;

  
  return;

}

void Promote::EndCycle() throw( SError ) {

  m_logger << INFO << "Number of selected events: " << m_n_selected_events << SLogger::endmsg;
  
  return;

}

void Promote::BeginInputData( const SInputData& ) throw( SError ) {

//   // initialize output variables
  m_o_is_selected_promote   = true;
  m_o_promote_weight        = 0.;
  m_o_combined_weight       = 0.;
  m_o_is_quality_promote     = new std::vector<bool>();
  m_o_is_quality              = new std::vector<bool>();
  m_o_n_quality_taus_promote = 0;
  m_o_n_quality_nonreal_taus_promote = 0;
  m_M_t                     = new std::vector<double>();


  // set object pointer
  m_tau_pt = 0;
  m_tau_prongness = 0;
  m_tau_phi = 0;
  m_tau_eta = 0;
//   m_evt_MT2 = 0;
//   m_evt_M_eff = 0;
  m_tau_phi = 0;
  m_tau_eta = 0;
  m_is_container = 0;
  m_is_container_20 = 0;
  m_is_loose = 0;
  m_is_medium = 0;
  m_is_true = 0;
  m_faketype = 0;
  m_faketype_full = 0;
//   m_tau_pt_1prong = 0;
//   m_tau_pt_3prong = 0;

  return;

}

void Promote::EndInputData( const SInputData& ) throw( SError ) {

  if (m_o_is_quality_promote) {
    delete m_o_is_quality_promote;
  }
  
   
  m_o_is_quality = 0;
  m_o_is_quality_promote = 0;
  m_o_n_quality_taus_promote = 0;
  m_o_n_quality_nonreal_taus_promote = 0;
  m_o_combined_weight = 0.;
  m_o_promote_weight = 0;
  
  std::cout << "weight 1prong: " << comparison_weight_1prong << "; eff 1prong: " << comparison_eff_1prong << "; weight 3prong: " << comparison_weight_3prong << "; eff 3prong: " << comparison_eff_3prong << std::endl;
  
  m_logger << INFO << "Counters for modified efficiencies, general: " << generalcounter << SLogger::endmsg;
  if (loweffcounter)
    m_logger << WARNING << "loweffcounter: " << loweffcounter << SLogger::endmsg;
  if (higheffcounter)
    m_logger << WARNING << "higheffcounter: " << higheffcounter << SLogger::endmsg;
  if (oneeffcounter)
    m_logger << WARNING << "oneeffcounter: " << oneeffcounter << SLogger::endmsg;
  
  return;

}

void Promote::LoadEffHistograms() {

  TFile* inf_hist = TFile::Open(m_c_pathhisteff.c_str());
  
  if (inf_hist == 0) {
    m_logger << ::FATAL << "Promote::LoadEffHistograms: Efficiency" << m_c_pathhisteff << "histogram not found!" << SLogger::endmsg;
    throw std::invalid_argument("Efficiency histogram not found.");
  }
  
  
    // retrieve efficiencies from input data
  uint nprong = 1;
  if (m_c_istaunu == true) {
    h_vec_effs.resize(2);
    for (TEffs*& h_effs: h_vec_effs) {
      h_effs = new TEffs();
      const char* effname = Form("Efficiency/h_eff_%dprong", nprong);
//       h_effs->h_eff = Retrieve<THeff>(effname, "Efficiency");
      h_effs->h_eff = dynamic_cast<THeff*>(inf_hist->Get(effname));
      if (!h_effs->h_eff) {
        m_logger << ::FATAL << "Promote::LoadEffHistograms: Efficiency histogram " << effname << " not found!" << SLogger::endmsg;
        throw std::invalid_argument("Efficiency histogram not found.");
      }
      h_effs->h_eff->SetDirectory(0);
      nprong = 3;
    }
  } else {
    h_vec_effs.resize(4);
    std::string match = "matched";
    uint sel = 0;
    for (TEffs*& h_effs: h_vec_effs) {
      h_effs = new TEffs();
      const char* effname = Form("Efficiency/h_eff_%dprong_%s", nprong, match.c_str());
//       h_effs->h_eff = Retrieve<THeff>(effname, "Efficiency");
      h_effs->h_eff = dynamic_cast<THeff*>(inf_hist->Get(effname));
      if (!h_effs->h_eff) {
        m_logger << ::FATAL << "Promote::LoadEffHistograms: Efficiency histogram " << effname << " not found!" << SLogger::endmsg;
        throw std::invalid_argument("Efficiency histogram not found.");
      }
      h_effs->h_eff->SetDirectory(0);
      ++sel;
      match = "antimatched";
      if (sel == 2) {
        nprong = 3;
        match = "matched";
      }
    }
  }
  
  inf_hist->Close();
  
//   TH1::SetDefaultSumw2();
//   TH2::SetDefaultSumw2();
}


void Promote::BeginInputFile( const SInputData& ) throw( SError ) {

  //
  // Connect the input variables:
  //
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_pt"                   , m_tau_pt                   );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_quality"              , m_tau_quality              );
 
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_prongness"            , m_tau_prongness            );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_phi"                  , m_tau_phi                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_eta"                  , m_tau_eta                  );
//   ConnectVariable( m_c_recoTreeName.c_str(), "_container_20"          , m_is_container_20          );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_container"             , m_is_container             );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_loose"                 , m_is_loose                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_medium"                , m_is_medium                );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_true"                  , m_is_true                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "faketype"                 , m_faketype                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "faketype_full"            , m_faketype_full            );
  ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_taus"              , m_n_cont_taus              );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_taus_20"           , m_n_cont_taus_20           );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_true_taus"              , m_n_true_taus              );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_taus"            , m_n_medium_taus            );
  ConnectVariable( m_c_recoTreeName.c_str(), "n_quality_taus"            , m_n_quality_taus            ); 
  ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_nonreal_taus"      , m_n_cont_nonreal_taus      );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_nonreal_taus"   , m_n_medium_nonreal_taus    );
//  ConnectVariable( m_c_recoTreeName.c_str(), "evt_MET"                  , m_met_mod                  );
//   ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_complete_bline", m_evt_weight_complete_bline);
//   ConnectVariable( m_recoTreeName.c_str(), "evt_MT2"                  , m_evt_MT2                  );
//   ConnectVariable( m_recoTreeName.c_str(), "evt_M_eff"                , m_evt_M_eff                );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_xSec"          , m_evt_weight_xSec          );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_nom"           , m_event_weight             );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_MET_phi"              , m_met_phi                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "mcid"                     , m_mcid                     );
//   ConnectVariable( m_c_recoTreeName.c_str(), "is_selected_measure"      , m_is_selected_measure      );
//   ConnectVariable( m_c_recoTreeName.c_str(), "M_eff"                    , m_M_eff                    );
//   ConnectVariable( m_c_recoTreeName.c_str(), "M_t"                      , m_M_t                      );
//   ConnectVariable( m_c_recoTreeName.c_str(), "delta_phi"                , m_delta_phi                );
//   ConnectVariable( m_c_recoTreeName.c_str(), "delta_eta"                , m_delta_eta                );
//   ConnectVariable( m_c_recoTreeName.c_str(), "tau_pt_1prong"            , m_tau_pt_1prong            );
//   ConnectVariable( m_c_recoTreeName.c_str(), "tau_pt_3prong"            , m_tau_pt_3prong            );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_MET_phi"              , m_evt_MET_phi              );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_eventNumber"          , m_evt_eventNumber          );
//  ConnectVariable( m_c_recoTreeName.c_str(), "electron_n"               , m_electron_n               );
//  ConnectVariable( m_c_recoTreeName.c_str(), "muon_n"                   , m_muon_n                   );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_taus_measure"    , m_n_medium_taus_measure    );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_nonreal_taus_measure"    , m_n_medium_nonreal_taus_measure    );
//   ConnectVariable( m_c_recoTreeName.c_str(), "is_medium_measure"                , m_is_medium_measure);
//   ConnectVariable( m_c_recoTreeName.c_str(), "is_elec_matched"          , m_is_elec_matched          );
//   ConnectVariable( m_c_recoTreeName.c_str(), "is_muon_matched"          , m_is_muon_matched          );
  if (!m_c_requiretrigger.empty())
    ConnectVariable( m_c_recoTreeName.c_str(), ("trig_" + m_c_requiretrigger).c_str(), m_triggerdecision);
    

  //
  // Declare the output variables: (needs to be done after ConnectVariable, for those branches at least which should be copied)
  //
//   DeclareVariable( *m_tau_pt,                "tau_pt"               );
DeclareVariable( *m_tau_quality,                "tau_quality"              );

//   DeclareVariable( *m_tau_prongness,         "tau_prongness"        );
//   DeclareVariable( *m_tau_phi,               "tau_phi"              );
//   DeclareVariable( *m_tau_eta,               "tau_eta"              );
// //   DeclareVariable( *m_is_container_20,       "is_container_20"      );
//   DeclareVariable( *m_is_container,          "is_container"         );
   DeclareVariable( *m_is_loose,              "is_loose"             );
//   DeclareVariable( *m_is_medium,             "is_medium"            );
//   DeclareVariable( *m_is_true,               "is_true"              );
//   DeclareVariable( *m_faketype,              "faketype"             );
//   DeclareVariable( *m_faketype_full,         "faketype_full"        );
//   DeclareVariable(  m_n_cont_taus,           "n_cont_taus"          );
// //   DeclareVariable(  m_n_cont_taus_20,        "n_cont_taus_20"       );
//   DeclareVariable(  m_n_true_taus,           "n_true_taus"          );
//   DeclareVariable(  m_n_medium_taus,         "n_medium_taus"        );
//   DeclareVariable(  m_n_quality_taus,         "n_quality_taus"        );
//   DeclareVariable(  m_n_quality_nonreal_taus,         "n_quality_taus"        );

   DeclareVariable(  m_n_cont_nonreal_taus,   "n_cont_nonreal_taus"  );
//   DeclareVariable(  m_n_medium_nonreal_taus, "n_medium_nonreal_taus");
//   DeclareVariable(  m_met_mod,               "met_mod"              );
// 
// //   DeclareVariable( *m_evt_MT2,               "evt_MT2"              );
// //   DeclareVariable( *m_evt_M_eff,             "evt_M_eff"            );
//   DeclareVariable(  m_n_baseline_el,         "n_baseline_el"        );
//   DeclareVariable(  m_n_baseline_mu,         "n_baseline_mu"        );
//   DeclareVariable(  m_pass_2eventcleaning,   "pass_2eventcleaning"  );
//   DeclareVariable(  m_pass_2baselineleptons, "pass_2baselineleptons");
//   DeclareVariable(  m_idx_promoted_ctau,     "idx_promoted_ctau"    );
   DeclareVariable(  m_event_weight,          "event_weight"         );
//   DeclareVariable(  m_met_phi,               "met_phi"              );
//   DeclareVariable(  m_evt_weight_xSec,       "evt_weight_xSec"      );
//   DeclareVariable(  m_is_selected_measure,   "is_selected_measure"  );
//   DeclareVariable(  m_M_eff,                 "M_eff"                );
//   DeclareVariable( *m_M_t,                   "M_t"                  );
//   DeclareVariable(  m_delta_phi,             "delta_phi"            );
//   DeclareVariable(  m_delta_eta,             "delta_eta"            );
// //   DeclareVariable( *m_tau_pt_1prong,         "tau_pt_1prong"        );
// //   DeclareVariable( *m_tau_pt_3prong,         "tau_pt_3prong"        );
//   DeclareVariable(  m_phi,                   "evt_MET_phi"          );
//   DeclareVariable(  m_evt_eventNumber,       "evt_eventNumber"      );
//   DeclareVariable(  m_electron_n,            "electron_n"           );
//   DeclareVariable(  m_muon_n,                "muon_n"               );
// //   DeclareVariable(  m_n_medium_taus_measure, "n_medium_taus_measure");
// //   DeclareVariable(  m_n_medium_nonreal_taus_measure, "n_medium_nonreal_taus_measure");
// //   DeclareVariable( *m_is_elec_matched,	     "is_elec_matched"      );
// //   DeclareVariable( *m_is_muon_matched,	     "is_muon_matched"      );

  // outputs generated here
  DeclareVariable( *m_o_is_quality,             "is_quality"            );

  
  DeclareVariable(  m_o_promote_weight,        "promote_weight"       );
  DeclareVariable(  m_o_promote_eff,           "promote_eff"          );
  DeclareVariable(  m_o_combined_weight,       "combined_weight"      );
  DeclareVariable(  m_o_is_selected_promote,   "is_selected_promote"  );
  DeclareVariable( *m_o_is_quality_promote,     "is_quality_promote"    );
  DeclareVariable(  m_o_nPhD,                  "nPhD"                 );
  DeclareVariable(  m_o_idx_phd,               "idx_phd"              );
  DeclareVariable(  m_o_n_quality_taus_promote, "n_quality_taus_promote");
  DeclareVariable(  m_o_n_quality_nonreal_taus_promote, "n_quality_nonreal_taus_promote");

  DeclareVariable(  m_o_quality_tau0_pt_promote,        "quality_tau0_pt_promote"              );
  DeclareVariable(  m_o_quality_tau1_pt_promote,        "quality_tau1_pt_promote"              );
  
  LoadEffHistograms();
  
  return;

}

void Promote::ParseConfiguration() {

  /// parse options
  /// MEAsure, PROmote, FUll faketype binning, FAKEtype binning, CONTainer tau binning, LEADing tau

  if (m_c_secondaxis.empty())
    m_secondaxis = kSANone;
  else if (m_c_secondaxis.find("fu") != std::string::npos)
    m_secondaxis = kFakeTypeFull;
  else if (m_c_secondaxis.find("fake") != std::string::npos)
    m_secondaxis = kFakeType;
  else if (m_c_secondaxis.find("cont1reco") != std::string::npos)
    m_secondaxis = knContTau1Reco;
  else if (m_c_secondaxis.find("cont") != std::string::npos)
    m_secondaxis = knContTau;
  else if (m_c_secondaxis.find("met") != std::string::npos)
    m_secondaxis = knMET;
  else if (m_c_secondaxis.find("prong") != std::string::npos)
    m_secondaxis = knProng;
  else
    throw("unknown");

  m_selectphd = kRandom;
//   if (option.find("lead") != std::string::npos)
//     m_selectphd = kLeading;

//   if (option.find("selev") != std::string::npos) {
//     int n = option.Index("selev") + 5;
//     m_selectevents = atoi(option(n, 5).Data());
//   }

}

bool Promote::IsEventSelected() {

  if (!m_c_requiretrigger.empty())
    if (!m_triggerdecision)
      return false;
  
  return true;

}

int Promote::GetSecondAxisBin(int tau_index) {

  // NOTE:
  // This does not actually return the bin number as in ROOT counting (0 = underflow), but the number to be used to pick the bin on the 2nd axis
  // (if it returns 0, use bin 1 on an axis going from -0.5 to something)
  // The exception is the MET binning where just the (capped) MET value is returned and a matching binning is provided in InitEff().
  int binno = 0;
  switch (m_secondaxis) {
  case kFakeType:
    binno = (*m_faketype)[tau_index];
    break;
  case kFakeTypeFull:
    binno = (*m_faketype_full)[tau_index];
    break;
  case knContTau:
    binno = m_n_cont_taus;
    break;
  case knContTau1Reco:
    exit(1);
//       if (Nrealreco() == 0)
//         binno = 0; // can reuse bin 0 in these cases (because for >=1 reco tau, bins 1.. are used: whenever there is a tau to be measured, n_cont_taus will be at least 1)
//       else
//         binno = m_n_cont_taus;
    break;
  case knMET:
    binno = m_met_mod;
    if (binno > m_c_bin_maxmet-1) // cap at m_c_bin_maxmet-1
      binno = m_c_bin_maxmet-1;
    break;
  case kSANone:
    binno = 0;
    break;
  case knProng:
    binno = (*m_tau_prongness)[tau_index];
    break;
  }
  return binno;

}


int Promote::GetThirdAxisBin(int tau_index) {

  // return corresponding index of vector h_vec_effs
  int nprong = (*m_tau_prongness)[tau_index];
  if (m_c_istaunu) {		// no matching for taunu sample
    if (nprong == 1)
      return 0;
    if (nprong == 3)
      return 1;
  } else {
    bool match;
    if (m_c_isenu) {
      match = (*m_is_elec_matched)[tau_index];
      if (nprong == 1 && match == true) 
	return 0;
      if (nprong == 1 && match == false)
	return 1;
      if (nprong == 3 && match == true)
	return 2;
      if (nprong == 3 && match == false)
	return 3;
    } else if (m_c_ismunu) {
      match = (*m_is_muon_matched)[tau_index];
      if (nprong == 1 && match == true) 
	return 0;
      if (nprong == 1 && match == false)
	return 1;
      if (nprong == 3 && match == true)
	return 2;
      if (nprong == 3 && match == false)
	return 3;
    }
  }
  m_logger << ::WARNING << "GetThirdAxisBin: Unexpected prongness " << nprong << SLogger::endmsg;
  return 0;

}


float Promote::GetEfficiency(int idx) {

  
  float efficiency;
  
//   if ((*m_tau_pt)[idx] < m_c_meanfakeeff_tauptcutoff || m_met_mod < m_c_meanfakeeff_metcutoff) { 		// determines whether fit or histogram is used
    TEffs* h_effs = h_vec_effs[GetThirdAxisBin(idx)];
    int binno = h_effs->h_eff->FindFixBin((*m_tau_pt)[idx], GetSecondAxisBin(idx));

//     std::cout << "idx: " << idx << "; h_eff: " << h_eff->GetBinContent(binno) << "; binno: " << binno << std::endl;
    while ((binno > 0) && (h_effs->h_eff->GetBinContent(binno) < 1e-5)) {
      --binno;
    }
    
  
    float bin_content = h_effs->h_eff->GetBinContent(binno);
    float bin_error = h_effs->h_eff->GetBinError(binno);
  
    if (m_c_efferror == "Max") {
      efficiency = bin_content + bin_error;
    }
    else if (m_c_efferror == "Min") {
      efficiency = bin_content - bin_error;
    }
    else {
      efficiency = bin_content;
    }  
    
    if (efficiency < 1e-6) {
      efficiency = (bin_content/3); // empty bin, return arbitrary average
      ++loweffcounter;
    }
    if (efficiency < 0) {
      efficiency = 0.015; // negative bin, set to arbitrary average
      ++negeffcounter;
    }
    if (efficiency == 1) {
      efficiency = 0.045; // unreasonably high fake efficiency, set to arbitrary average
      ++oneeffcounter;
    }
    if (efficiency > 0.9999) {
      efficiency = bin_content + 2*(1-bin_content)/3;
      ++higheffcounter;
    }
    ++generalcounter;
    
//   }
//   else {
//     if (GetThirdAxisBin(idx) == 0) {	// 1prong
//       if (m_c_efferror == "Max") {
// 	efficiency = 0.073 + 0.003 + (19e-8 + 4e-8) * (*m_tau_pt)[idx];
//       }
//       else if (m_c_efferror == "Min") {
// 	efficiency = 0.073 - 0.003 + (19e-8 - 4e-8) * (*m_tau_pt)[idx];
//       }
//       else {
// // 	efficiency = 0.073 + (19e-8) * (*m_tau_pt)[idx];
// // 	efficiency = 0.079;
// 	efficiency = 0.0137;
//       }
//     }
//     else if (GetThirdAxisBin(idx) == 1) {
//       if (m_c_efferror == "Max") {
// 	efficiency = 0.0047 + 0.0002;
//       }
//       else if (m_c_efferror == "Min") {
// 	efficiency = 0.0047 - 0.0002;
//       }
//       else {
// // 	efficiency = 0.0051;
// // 	efficiency = 0.0027;
// 	efficiency = 0.009;
//       }
//     }
//     else {
//       // should never be reached
//       efficiency = 0.045;
//     }
//   }
  return efficiency;
}

float Promote::DoPromote() {

  std::vector<unsigned int> phds;
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    if (m_is_true->at(idx))
      continue;
    if (!m_is_container->at(idx))
      continue;
    if ((*m_tau_pt)[idx] < m_c_mintaupt)
      continue;
    if (Is_tau_quality(idx))
      continue;
    if (!m_c_isunmatched) {
      if (m_c_isenu) {
//       if ((*m_tau_prongness)[idx] == 1 || (*m_tau_prongness)[idx] == 3)
	if (m_c_ismatched_sign) {
	  if (m_c_ismatched) {
	    if (!m_is_elec_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (m_is_elec_matched->at(idx)) {
	      continue;
	    }
	  }
	} else {
	  if (m_c_ismatched) {
	    if (m_is_elec_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (!m_is_elec_matched->at(idx)) {
	      continue;
	    }
	  }
	}
      }
      if (m_c_ismunu) {
//       if ((*m_tau_prongness)[idx] == 1 || (*m_tau_prongness)[idx] == 3)
	if (m_c_ismatched_sign) {
	  if (m_c_ismatched) {
	    if (!m_is_muon_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (m_is_muon_matched->at(idx)) {
	      continue;
	    }
	  }
	} else {
	  if (m_c_ismatched) {
	    if (m_is_muon_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (!m_is_muon_matched->at(idx)) {
	      continue;
	    }
	  }
	}
      }
    }
    phds.push_back(idx);
  }
  m_o_nPhD = phds.size();
  
  m_o_idx_phd = 0;
  if (phds.size() == 0) {
    m_o_promote_weight = 0.;
    return m_o_promote_weight;
  }

  if (m_selectphd == kLeading) {
    m_o_idx_phd = 0;
    for (uint i = 0; i < phds.size(); ++i) {
      if ((*m_tau_pt)[phds[i]] > (*m_tau_pt)[phds[m_o_idx_phd]])
        m_o_idx_phd = i;
    }
  } else {
    do {
      m_o_idx_phd = drand48()*phds.size();
    } while (m_o_idx_phd == phds.size());
  }

  uint idx = phds[m_o_idx_phd];
  m_o_promote_eff = GetEfficiency(idx);
  
  // need to recompute m_n_medium_nonreal_taus_measure here (TODO: check if same result)
  Int_t m_n_quality_nonreal_taus_measure = 0;
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) { 
    if (Is_tau_quality(idx)) {
      if((*m_tau_pt)[idx] > m_c_mintaupt) {
        if((*m_is_true)[idx] == false) {
          ++m_n_quality_nonreal_taus_measure;
        }
      }
    }  
  }

  m_o_promote_weight = (m_o_promote_eff/(1-m_o_promote_eff)) * phds.size() / (m_n_quality_nonreal_taus_measure + 1);
  m_o_combined_weight = m_event_weight * m_o_promote_weight;
  (*m_o_is_quality_promote)[idx] = true;

  if ((*m_tau_pt)[idx] > m_c_meanfakeeff_tauptcutoff && m_met_mod > m_c_meanfakeeff_metcutoff) {
//     std::cout << "mcid: " << m_mcid << "; Eventnumber: " << m_evt_eventNumber << "; promote weight: " << m_o_promote_weight << "; efficiency: " << eff << "; tau_pt: " << (*m_tau_pt)[idx] << std::endl;
    if((*m_tau_prongness)[idx] == 1) {
      float test = (0.079/(1-0.079)) * phds.size() / (m_n_quality_nonreal_taus_measure + 1);
      comparison_weight_1prong += m_event_weight * (m_o_promote_weight - test);
      comparison_eff_1prong += m_o_promote_eff - 0.079;
//       std::cout << "1prong meaneff weight: " << test << std::endl;
    }
    else if ((*m_tau_prongness)[idx] == 3) {
      float test = (0.0027/(1-0.0027)) * phds.size() / (m_n_quality_nonreal_taus_measure + 1);
      comparison_weight_3prong += m_event_weight * (m_o_promote_weight - test);
      comparison_eff_3prong += m_o_promote_eff - 0.0027;
//       std::cout << "3prong meaneff weight: " << test << std::endl;
    }
  }

  return m_o_promote_weight;

}



void Promote::DoCount() {
  
//   (*m_o_is_medium_promote).assign(m_is_medium->begin(), m_is_medium->end());
// 
//   ComputePromoteWeight();

  double abs_tauE = 0.;			// absolute value of pt for event
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    abs_tauE += (*m_tau_pt)[idx] * (*m_tau_pt)[idx];  // computation can't be done in same loop as use
  }
  abs_tauE = sqrt(abs_tauE);

  m_M_t->resize(m_tau_pt->size()); // this was missing in Marco's code (and explains why he never got good results for M_T...)
  
  Float_t quality_tau0_pt_promote = -1.;
  Float_t quality_tau1_pt_promote = -1.;
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    
    if ((*m_o_is_quality_promote)[idx]) {		// count number of [quality] taus again after promotion
      if ((*m_tau_pt)[idx] > m_c_mintaupt) {
        ++m_o_n_quality_taus_promote;
        if ((*m_is_true)[idx] == false) {
          ++m_o_n_quality_nonreal_taus_promote;
        }
      } else {
        (*m_o_is_quality_promote)[idx] = false;
      }
      // find (sub-)leading tau pt
      if ((*m_tau_pt)[idx] > quality_tau0_pt_promote) {
        quality_tau1_pt_promote = quality_tau0_pt_promote;
        quality_tau0_pt_promote = (*m_tau_pt)[idx];
      } else if ((*m_tau_pt)[idx] > quality_tau1_pt_promote) {
        quality_tau1_pt_promote = (*m_tau_pt)[idx];
      }
    }
    
    (*m_M_t)[idx] = sqrt(2*abs_tauE*m_met_mod*(1-cos(m_evt_MET_phi - (*m_tau_phi)[idx])));	// transverse mass
//     if (m_tau_prongness->at(idx) == 1) {
//       m_tau_pt_1prong->push_back(m_tau_pt->at(idx));
//     }
//     if (m_tau_prongness->at(idx) == 3) {
//       m_tau_pt_3prong->push_back((*m_tau_pt)[idx]);
//     }
    
    
  }
  
  m_o_quality_tau0_pt_promote = quality_tau0_pt_promote;
  m_o_quality_tau1_pt_promote = quality_tau1_pt_promote;

//   int meds = m_n_medium_taus - m_o_n_medium_taus_promote;
//   if (meds != 0 ) {
//     m_logger << ::DEBUG << "old - new meds: " << m_n_medium_taus << " - " << m_o_n_medium_taus_promote << " = " << meds << SLogger::endmsg;
//   }
//   m_logger << ::DEBUG << "number phds: " << m_o_nPhD << SLogger::endmsg;
//   m_logger << ::DEBUG << "old - new meds: " << m_n_medium_taus << " - " << m_o_n_medium_taus_promote << " = " << meds << SLogger::endmsg;
  
  
  // FIXME: this doesn't respect is_medium!
/*  
    // M_eff, delta_eta, delta_phi need sorted pt vector; therefore clone and sort clone
  std::vector<float> *pt_helper = new std::vector<float>(m_tau_pt->begin(), m_tau_pt->end()); 
  std::sort(pt_helper->begin(), pt_helper->end(), std::greater<double>());
  
  
  // Fill output branches without selection
  m_M_eff = m_met_mod;					// effective mass
  if (m_tau_pt->size() >= 1) {
    m_M_eff += (*pt_helper)[0];
  }
  if (m_tau_pt->size() >= 2) {
    m_M_eff += (*pt_helper)[1];
  }
*/  
    // currently not working
//   int count = 0;
//   for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
//     if (m_is_true->at(idx) && count == 0) {
// 
//       // catch leading tau as reference, if leading tau is selected catch subleading instead
//       int which_tau = 0;
//       if (idx == 0) {
//         which_tau = 1;
//       }
//       m_delta_phi = (*m_tau_phi)[idx] - (*m_tau_phi)[which_tau];			// azimutal angle
//       m_delta_eta = (*m_tau_eta)[idx] - (*m_tau_eta)[which_tau];			// pseudorapidity
//       ++count;
//     }
//   }
}




void Promote::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {

//   m_tau_pt_1prong->clear();
//   m_tau_pt_3prong->clear();
//  m_o_is_quality_promote->clear();
//  (*m_o_is_quality_promote).assign(m_is_medium->begin(), m_is_medium->end());
  
    (*m_o_is_quality_promote).resize(m_tau_pt->size());
for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    (*m_o_is_quality_promote)[idx] = Is_tau_quality(idx);
}
 
     (*m_o_is_quality).resize(m_tau_pt->size());
for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    (*m_o_is_quality)[idx] = Is_tau_quality(idx);
}
  


  m_o_n_quality_taus_promote = 0;
  m_o_n_quality_nonreal_taus_promote = 0;
  m_o_idx_phd = 0;
  m_o_nPhD = 0;
  m_o_promote_weight = 0.;
  m_o_promote_eff = 0.;
  m_o_combined_weight = 0.;
  m_o_quality_tau0_pt_promote        = -1.;
  m_o_quality_tau1_pt_promote        = -1.;
  
  if ((m_o_is_selected_promote = IsEventSelected())) {
    ++m_n_selected_events;
    if (m_c_istaunu) {
      DoPromote();

    } else {
      if (m_electron_n >= 1 || m_muon_n >= 1) {
        DoPromote();
      }
    }
  }
  
  DoCount();
}

