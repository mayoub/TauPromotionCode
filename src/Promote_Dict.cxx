// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME srcdIPromote_Dict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "include/Measure.h"
#include "include/Promote.h"
#include "include/TruthMatch.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_Measure(void *p = 0);
   static void *newArray_Measure(Long_t size, void *p);
   static void delete_Measure(void *p);
   static void deleteArray_Measure(void *p);
   static void destruct_Measure(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Measure*)
   {
      ::Measure *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Measure >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Measure", ::Measure::Class_Version(), "include/Measure.h", 42,
                  typeid(::Measure), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Measure::Dictionary, isa_proxy, 4,
                  sizeof(::Measure) );
      instance.SetNew(&new_Measure);
      instance.SetNewArray(&newArray_Measure);
      instance.SetDelete(&delete_Measure);
      instance.SetDeleteArray(&deleteArray_Measure);
      instance.SetDestructor(&destruct_Measure);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Measure*)
   {
      return GenerateInitInstanceLocal((::Measure*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Measure*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_Promote(void *p = 0);
   static void *newArray_Promote(Long_t size, void *p);
   static void delete_Promote(void *p);
   static void deleteArray_Promote(void *p);
   static void destruct_Promote(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Promote*)
   {
      ::Promote *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Promote >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Promote", ::Promote::Class_Version(), "include/Promote.h", 29,
                  typeid(::Promote), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Promote::Dictionary, isa_proxy, 4,
                  sizeof(::Promote) );
      instance.SetNew(&new_Promote);
      instance.SetNewArray(&newArray_Promote);
      instance.SetDelete(&delete_Promote);
      instance.SetDeleteArray(&deleteArray_Promote);
      instance.SetDestructor(&destruct_Promote);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Promote*)
   {
      return GenerateInitInstanceLocal((::Promote*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Promote*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TruthMatch(void *p = 0);
   static void *newArray_TruthMatch(Long_t size, void *p);
   static void delete_TruthMatch(void *p);
   static void deleteArray_TruthMatch(void *p);
   static void destruct_TruthMatch(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TruthMatch*)
   {
      ::TruthMatch *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TruthMatch >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TruthMatch", ::TruthMatch::Class_Version(), "include/TruthMatch.h", 17,
                  typeid(::TruthMatch), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TruthMatch::Dictionary, isa_proxy, 4,
                  sizeof(::TruthMatch) );
      instance.SetNew(&new_TruthMatch);
      instance.SetNewArray(&newArray_TruthMatch);
      instance.SetDelete(&delete_TruthMatch);
      instance.SetDeleteArray(&deleteArray_TruthMatch);
      instance.SetDestructor(&destruct_TruthMatch);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TruthMatch*)
   {
      return GenerateInitInstanceLocal((::TruthMatch*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TruthMatch*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr Measure::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Measure::Class_Name()
{
   return "Measure";
}

//______________________________________________________________________________
const char *Measure::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Measure*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Measure::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Measure*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Measure::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Measure*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Measure::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Measure*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Promote::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Promote::Class_Name()
{
   return "Promote";
}

//______________________________________________________________________________
const char *Promote::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Promote*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Promote::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Promote*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Promote::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Promote*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Promote::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Promote*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TruthMatch::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TruthMatch::Class_Name()
{
   return "TruthMatch";
}

//______________________________________________________________________________
const char *TruthMatch::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TruthMatch*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TruthMatch::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TruthMatch*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TruthMatch::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TruthMatch*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TruthMatch::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TruthMatch*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void Measure::Streamer(TBuffer &R__b)
{
   // Stream an object of class Measure.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Measure::Class(),this);
   } else {
      R__b.WriteClassBuffer(Measure::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Measure(void *p) {
      return  p ? new(p) ::Measure : new ::Measure;
   }
   static void *newArray_Measure(Long_t nElements, void *p) {
      return p ? new(p) ::Measure[nElements] : new ::Measure[nElements];
   }
   // Wrapper around operator delete
   static void delete_Measure(void *p) {
      delete ((::Measure*)p);
   }
   static void deleteArray_Measure(void *p) {
      delete [] ((::Measure*)p);
   }
   static void destruct_Measure(void *p) {
      typedef ::Measure current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Measure

//______________________________________________________________________________
void Promote::Streamer(TBuffer &R__b)
{
   // Stream an object of class Promote.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Promote::Class(),this);
   } else {
      R__b.WriteClassBuffer(Promote::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Promote(void *p) {
      return  p ? new(p) ::Promote : new ::Promote;
   }
   static void *newArray_Promote(Long_t nElements, void *p) {
      return p ? new(p) ::Promote[nElements] : new ::Promote[nElements];
   }
   // Wrapper around operator delete
   static void delete_Promote(void *p) {
      delete ((::Promote*)p);
   }
   static void deleteArray_Promote(void *p) {
      delete [] ((::Promote*)p);
   }
   static void destruct_Promote(void *p) {
      typedef ::Promote current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Promote

//______________________________________________________________________________
void TruthMatch::Streamer(TBuffer &R__b)
{
   // Stream an object of class TruthMatch.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TruthMatch::Class(),this);
   } else {
      R__b.WriteClassBuffer(TruthMatch::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TruthMatch(void *p) {
      return  p ? new(p) ::TruthMatch : new ::TruthMatch;
   }
   static void *newArray_TruthMatch(Long_t nElements, void *p) {
      return p ? new(p) ::TruthMatch[nElements] : new ::TruthMatch[nElements];
   }
   // Wrapper around operator delete
   static void delete_TruthMatch(void *p) {
      delete ((::TruthMatch*)p);
   }
   static void deleteArray_TruthMatch(void *p) {
      delete [] ((::TruthMatch*)p);
   }
   static void destruct_TruthMatch(void *p) {
      typedef ::TruthMatch current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TruthMatch

namespace {
  void TriggerDictionaryInitialization_Promote_Dict_Impl() {
    static const char* headers[] = {
"include/Measure.h",
"include/Promote.h",
"include/TruthMatch.h",
0
    };
    static const char* includePaths[] = {
"/project/etp/Samara/SFrameTest/SFrame",
"./",
"/software/opt/xenial/x86_64/root/6.08.00/include",
"/project/etp/Samara/SFrameTest/SFrame/Promote/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "Promote_Dict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$include/Measure.h")))  Measure;
class __attribute__((annotate("$clingAutoload$include/Promote.h")))  Promote;
class __attribute__((annotate("$clingAutoload$include/TruthMatch.h")))  TruthMatch;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "Promote_Dict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "include/Measure.h"
#include "include/Promote.h"
#include "include/TruthMatch.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Measure", payloadCode, "@",
"Promote", payloadCode, "@",
"TruthMatch", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("Promote_Dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_Promote_Dict_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_Promote_Dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_Promote_Dict() {
  TriggerDictionaryInitialization_Promote_Dict_Impl();
}
