// $Id: Measure.cxx 2686 2016-12-18 21:21:22Z eis $

// Local include(s):
#include "../include/Measure.h"
#include <cmath>
#include <iostream>
#include <TH2.h>
#include <TH1.h>
#include <algorithm>
#include <array>
#include <fstream>
#include <iomanip>
#include <TList.h>

std::string Measure::m_c_recoTreeName;
std::string Measure::m_c_metaTreeName;
std::string Measure::m_c_secondaxis;
double Measure::m_c_bin_maxmet;
double Measure::m_c_mintaupt;
double Measure::m_c_meanfakeeff_tauptcutoff;
bool Measure::m_c_isenu;
bool Measure::m_c_ismunu;
bool Measure::m_c_istaunu;
bool Measure::m_c_ismatched;
bool Measure::m_c_ismatched_sign;
bool Measure::m_c_isunmatched;
bool Measure::m_c_select1tauevents;
int Measure::m_c_selecttautype; 

// bool Measure::m_c_switch; // 
std::string Measure::m_c_requiretrigger;
  
uint m_count_1prong = 0; // used for counting over more than one event
uint dummy1_2 = 0;
uint m_count_3prong = 0;
uint dummy3_2 = 0;

uint counter1 = 0;
uint counter3 = 0;

uint tau_nr = 0;
uint fake_nr = 0;
uint event_nr = 0;


std::vector<int> faketype_counter_1prong(11);
std::vector<int> faketype_counter_3prong(11);
std::vector<int> faketype_counter_1prong_d(11);
std::vector<int> faketype_counter_3prong_d(11);



ClassImp( Measure );

Measure::Measure(): 
  SCycleBase()
  {

  DeclareProperty( "RecoTreeName", m_c_recoTreeName );
  DeclareProperty( "MetaTreeName", m_c_metaTreeName );
  
  DeclareProperty( "SecondAxis"  , m_c_secondaxis );
  DeclareProperty( "MaxMET"      , m_c_bin_maxmet = 500000. );
  DeclareProperty( "MinTauPt"    , m_c_mintaupt = 2e4 );
  DeclareProperty( "TauPtCutOff" , m_c_meanfakeeff_tauptcutoff = 0);		// should equal m_c_meanfakeeff_tauptcutoff in Promote.cxx; set to zero if no cut is used
  DeclareProperty( "Ise"         , m_c_isenu = false);				// Sets additional selection for e samples, primarily used for matching
  DeclareProperty( "Ismu"        , m_c_ismunu = false);				// same for muons
  DeclareProperty( "Istau"       , m_c_istaunu = true);			// disables matching and preselection for e and mu
  DeclareProperty( "IsMatched"   , m_c_ismatched = true);			// switch between matching (true, only cont taus matched with e/mu are used for eff and prom) and antimatching (false)
  DeclareProperty( "IsMatchedSign",m_c_ismatched_sign = true);			// swaps matched and antimatched
  DeclareProperty( "IsUnmatched" , m_c_isunmatched = true);                     // disables matched/antimatched
  DeclareProperty( "Select1TauEvents" , m_c_select1tauevents = false);
  DeclareProperty( "SelectTauType" , m_c_selecttautype = 2);

  DeclareProperty( "RequireTrigger", m_c_requiretrigger = "");
  
  SetLogName( GetName() );
  // m_config.SetPrintEveryEvents(50000);
  m_n_selected_events = 0;
  
}

Measure::~Measure() {

}

  void Measure::BeginCycle() throw( SError ) {

  ParseConfiguration();
  
  m_logger << INFO << "Minimum tau pt: "  << m_c_mintaupt << SLogger::endmsg;
  m_logger << INFO << "m_secondaxis: "  << m_secondaxis << SLogger::endmsg;
  m_logger << INFO << "SelectTauType: " << m_c_selecttautype << SLogger::endmsg;

  return;

}

void Measure::EndCycle() throw( SError ) {
  
  m_logger << INFO << "Number of selected events: " << m_n_selected_events << SLogger::endmsg;
  
  return;

}



void Measure::InitializeHistograms() {
  
  uint nprong = 1;
  if (m_c_istaunu == true) {
    h_vec_effs.resize(2);
    for (TEffs*& h_effs: h_vec_effs) {
      h_effs = new TEffs();
      InitializeEffHistograms(h_effs, Form("_%dprong", nprong));
      nprong = 3;
    }
  } else {
    std::string match = "matched";
    uint sel = 0;
    h_vec_effs.resize(4);
    for (TEffs*& h_effs: h_vec_effs) {
      h_effs = new TEffs();
      InitializeEffHistograms(h_effs, Form("_%dprong_%s", nprong, match.c_str()));
      ++sel;
      match = "antimatched";
      if (sel == 2) {
	nprong = 3;
	match = "matched";
      }
    }
  }
}

// Float_t* Measure::SetBinning(Float_t* binwidths, Float_t* bincount) {
std::vector<Float_t> Measure::SetBinning(std::vector<Float_t> bwidths, std::vector<Float_t> bcount) {
  float offset = 0.;
  std::vector<Float_t> binrange;
  binrange.push_back(offset*1e3);
  
//   bincount.back() = bincount.back() + 1; // TODO:CHECK if necessary

  for (uint i = 0; i < bcount.size(); ++i) {
    float bc = bcount[i];
    float bw = bwidths[i];
    for (uint j = 0; j < bc; ++j) {
      offset += bw;
      binrange.push_back(offset*1e3);
    }
  }
  return binrange;
}


void Measure::InitializeEffHistograms(TEffs* h_effs, const TString& varname) {
  
  m_logger << ::INFO << "Creating efficiency histograms for variant " << varname << SLogger::endmsg;
  
  int nbinsy = 7;
  float ymin = -0.5;
  float ymax = nbinsy-.5;
  
  if (m_secondaxis == kFakeTypeFull)
    nbinsy = 30;
  else if (m_secondaxis == knMET) {
    nbinsy = 7;
    ymin = 0;
    ymax = m_c_bin_maxmet;
  } else if (m_secondaxis == kFakeType) {
    nbinsy = 11;
    ymax = nbinsy - .5;
  }
  
//   int pt_nbinsx_1 = 7;
//   int pt_nbinsx_2 = 15;
//   int pt_nbinsx = pt_nbinsx_1 + pt_nbinsx_2 + 1;
//   double pt_abinsx[pt_nbinsx];
//   int pt_rangex_1 = 175;
//   int pt_rangex_2 = 600;
//   int pt_cutoff = m_c_bin_maxmet/1e3;
//   
//   pt_abinsx[0] = 0; 
//   for (int idx = 0; idx < pt_nbinsx_1; ++idx) {
//     pt_abinsx[idx + 1] = (pt_cutoff + pt_rangex_1/pt_nbinsx_1*idx)*1e3;
//   }
//   for (int idx = 0; idx < (pt_nbinsx_2+1); ++idx) {
//     pt_abinsx[pt_nbinsx_1 + idx + 1] = (pt_cutoff + pt_rangex_1 + pt_rangex_2/pt_nbinsx_2*idx)*1e3;
//   }
//   
//   int met_nbinsx_1 = 7;
//   int met_nbinsx_2 = 11;
//   int met_nbinsx = met_nbinsx_1 + met_nbinsx_2;
//   double met_abinsx[met_nbinsx];
//   int met_rangex_1 = 175;
//   int met_rangex_2 = 440;
//   
//   for (int idx = 0; idx < met_nbinsx_1; ++idx) {
//     met_abinsx[idx] = (met_rangex_1/met_nbinsx_1*idx)*1e3;
//   }
//   for (int idx = 0; idx < (met_nbinsx_2+1); ++idx) {
//     met_abinsx[met_nbinsx_1 + idx] = (met_rangex_1 + met_rangex_2/met_nbinsx_2*idx)*1e3;
//   }

  // specs:
  // default: {20, 50} - {10, 6}
  // taunu: {10, 20, 30, 60, 300} - {6, 1, 2, 1, 1} == standard
  // enu: 1prong matched:{50, 100, 350} - {1, 1, 1}, 3prong antimatched {10, 20, 30, 40, 350} - {6, 1, 1, 1, 1}
  // munu: 3prong antimatched {10, 20, 30, 40, 350} - {6, 1, 1, 1, 1}, 3prong matched {10, 20, 40, 60, 350} - {3, 1, 1, 1, 1}
  
  
  
  Float_t binwidths_helper[] = {10, 20, 30, 60, 300};
  Float_t bincount_helper[] = {6, 1, 2, 1, 1};
//   std::vector<Float_t> binwidths (binwidths_helper, binwidths_helper + sizeof(binwidths_helper)/sizeof(binwidths_helper[0]));
//   std::vector<Float_t> bincount (bincount_helper, bincount_helper + sizeof(bincount_helper)/sizeof(bincount_helper[0]));
  
//   std::vector<Float_t> binrange_vec = Measure::SetBinning(binwidths, bincount);
  std::vector<Float_t> binrange_vec;
//   if(varname == "_3prong_matched"){
//     Float_t binwidths_helper[] = {10, 20, 40, 60, 350};
//     Float_t bincount_helper[] = {3, 1, 1, 1, 1};
//     std::vector<Float_t> binwidths (binwidths_helper, binwidths_helper + sizeof(binwidths_helper)/sizeof(binwidths_helper[0]));
//     std::vector<Float_t> bincount (bincount_helper, bincount_helper + sizeof(bincount_helper)/sizeof(bincount_helper[0]));
//     binrange_vec = Measure::SetBinning(binwidths, bincount);
//   }else if (varname == "_3prong_antimatched") {
//     Float_t binwidths_helper[] = {10, 20, 30, 40, 350};
//     Float_t bincount_helper[] = {6, 1, 1, 1, 1};
//     std::vector<Float_t> binwidths (binwidths_helper, binwidths_helper + sizeof(binwidths_helper)/sizeof(binwidths_helper[0]));
//     std::vector<Float_t> bincount (bincount_helper, bincount_helper + sizeof(bincount_helper)/sizeof(bincount_helper[0]));
//     binrange_vec = Measure::SetBinning(binwidths, bincount);
//   }else {
//     Float_t binwidths_helper[] = {20, 50};
//     Float_t bincount_helper[] = {10, 6};
  
    std::vector<Float_t> binwidths (binwidths_helper, binwidths_helper + sizeof(binwidths_helper)/sizeof(binwidths_helper[0]));
    std::vector<Float_t> bincount (bincount_helper, bincount_helper + sizeof(bincount_helper)/sizeof(bincount_helper[0]));
    binrange_vec = Measure::SetBinning(binwidths, bincount);
//   }
  int nbinsx = binrange_vec.size() - 1;
  Double_t binrange[binrange_vec.size()];
  std::copy(binrange_vec.begin(), binrange_vec.end(), binrange);
  

  // efficiency in pt
//   if (m_c_istaunu) {
//     h_effs->h_u   = Book( THeff("h_eff_u" + varname, "h_eff_u", nbinsx, binrange, nbinsx, binrange), "Efficiency" );
//     h_effs->h_d   = Book( THeff("h_eff_d" + varname, "h_eff_d", nbinsx, binrange, nbinsx, binrange), "Efficiency" );
//   
//     h_effs->h_eff = Book( THeff("h_eff"   + varname, "h_eff"  , nbinsx, binrange, nbinsx, binrange), "Efficiency" );
//   } else {
    h_effs->h_u   = Book( THeff("h_eff_u" + varname, "h_eff_u", nbinsx, binrange, nbinsy, ymin, ymax), "Efficiency" );
    h_effs->h_d   = Book( THeff("h_eff_d" + varname, "h_eff_d", nbinsx, binrange, nbinsy, ymin, ymax), "Efficiency" );
  
    h_effs->h_eff = Book( THeff("h_eff"   + varname, "h_eff"  , nbinsx, binrange, nbinsy, ymin, ymax), "Efficiency" );
//   }
    
    
  // efficiency in MET
  h_effs->h_met_u   = Book( THeff("h_eff_met_u" + varname, "h_eff_met_u", nbinsx, binrange, nbinsy, ymin, ymax), "Efficiency" );
  h_effs->h_met_d   = Book( THeff("h_eff_met_d" + varname, "h_eff_met_d", nbinsx, binrange, nbinsy, ymin, ymax), "Efficiency" );
  h_effs->h_met_eff = Book( THeff("h_eff_met"   + varname, "h_eff_met"  , nbinsx, binrange, nbinsy, ymin, ymax), "Efficiency" );
  
//   h_effs->h_met_u   = Book( THeff("h_eff_met_u" + varname, "h_eff_met_u", nbinsx, binrange, nbinsx, binrange), "Efficiency" );
//   h_effs->h_met_d   = Book( THeff("h_eff_met_d" + varname, "h_eff_met_d", nbinsx, binrange, nbinsx, binrange), "Efficiency" );
//   h_effs->h_met_eff = Book( THeff("h_eff_met"   + varname, "h_eff_met"  , nbinsx, binrange, nbinsx, binrange), "Efficiency" );
  
  // efficiency in pt only
  h_effs->h_ptonly_d   = Book( TH1F("h_eff_ptonly_d" + varname, "h_eff_ptonly_d", nbinsx, binrange), "Efficiency");
  h_effs->h_ptonly_u   = Book( TH1F("h_eff_ptonly_u" + varname, "h_eff_ptonly_u", nbinsx, binrange), "Efficiency");
  h_effs->h_ptonly_eff = Book( TH1F("h_eff_ptonly" + varname, "h_eff_ptonly", nbinsx, binrange), "Efficiency");
  
  // efficiency in met only
  h_effs->h_metonly_d   = Book( TH1F("h_eff_metonly_d" + varname, "h_eff_metonly_d", nbinsx, binrange), "Efficiency");
  h_effs->h_metonly_u   = Book( TH1F("h_eff_metonly_u" + varname, "h_eff_metonly_u", nbinsx, binrange), "Efficiency");
  h_effs->h_metonly_eff = Book( TH1F("h_eff_metonly" + varname, "h_eff_metonly", nbinsx, binrange), "Efficiency");
  
  // efficiency in different faketypes
//   if (m_secondaxis == kFakeType) {
//     h_effs->h_qjetsonly_eff = Book( TH1D("h_eff_qjetsonly" + varname, "h_eff_qjetsonly", nbinsx, binrange), "Efficiency");
//     h_effs->h_gjetsonly_eff = Book( TH1D("h_eff_gjetsonly" + varname, "h_eff_gjetsonly", nbinsx, binrange), "Efficiency");
//     h_effs->h_hfonly_eff = Book( TH1D("h_eff_hfonly" + varname, "h_eff_hfonly", nbinsx, binrange), "Efficiency");
//     h_effs->h_jetsonly_eff = Book( TH1D("h_eff_jetsonly" + varname, "h_eff_jetsonly", nbinsx, binrange), "Efficiency");
//     
//     h_effs->h_eleconly_eff = Book( TH1D("h_eff_eleconly" + varname, "h_eff_eleconly", nbinsx, binrange), "Efficiency");
//     h_effs->h_convonly_eff = Book( TH1D("h_eff_convonly" + varname, "h_eff_convonly", nbinsx, binrange), "Efficiency");
//     h_effs->h_electronsonly_eff = Book( TH1D("h_eff_electronsonly" + varname, "h_eff_electronsonly", nbinsx, binrange), "Efficiency");
//   }
  
  
  switch (m_secondaxis) {
    case kFakeType:
      h_effs->h_eff->GetYaxis()->SetTitle("fake type");
      h_effs->h_met_eff->GetYaxis()->SetTitle("fake type");
      break;
    case kFakeTypeFull:
      h_effs->h_eff->GetYaxis()->SetTitle("fake type (full set)");
      h_effs->h_met_eff->GetYaxis()->SetTitle("fake type (full set)");
      break;
    case knContTau:
      h_effs->h_eff->GetYaxis()->SetTitle("# container taus");
      h_effs->h_met_eff->GetYaxis()->SetTitle("# container taus");
      break;
    case knContTau1Reco:
      h_effs->h_eff->GetYaxis()->SetTitle("0 reco / n container taus");
      h_effs->h_met_eff->GetYaxis()->SetTitle("0 reco / n container taus");
      break;
    case knMET:
      h_effs->h_eff->GetYaxis()->SetTitle("MET [MeV]");
      h_effs->h_met_eff->GetYaxis()->SetTitle("MET [MeV]"); // ignore in this case, not meaningful
      break;
    case knProng:
      h_effs->h_eff->GetYaxis()->SetTitle("# prongs");
      h_effs->h_met_eff->GetYaxis()->SetTitle("# prongs");
      break;
    default:
      ;
  }
  h_effs->h_eff->GetXaxis()->SetTitle("tau p_{T} [MeV]");
  h_effs->h_met_eff->GetXaxis()->SetTitle("E^{miss}_{T} [MeV]");
  
//   TH1::SetDefaultSumw2();
//   TH2::SetDefaultSumw2();
  
  


}




void Measure::BeginInputData( const SInputData& id) throw( SError ) {
  
  m_logger << ::INFO << "Setting up histograms..." << SLogger::endmsg;
  InitializeHistograms();

  // initialize output variables
  m_o_M_t               = new std::vector<double>();
  m_o_tau_pt_1prong     = new std::vector<double>();
  m_o_tau_pt_3prong     = new std::vector<double>();
  m_o_is_quality_measure = new std::vector<bool>();
  m_o_is_quality         = new std::vector<bool>();

  m_o_is_elec_matched   = new std::vector<bool>();
  m_o_is_muon_matched   = new std::vector<bool>();
  m_o_electron_LorentzVec = new std::vector<TLorentzVector>();
  m_o_muon_LorentzVec   = new std::vector<TLorentzVector>();
  m_o_tau_LorentzVec    = new std::vector<TLorentzVector>();
  
  m_o_M_eff 	 	    = 0.;
  m_o_delta_eta 	    = 0.;
  m_o_delta_phi 	    = 0.;
  m_o_n_quality_taus_measure = 0; 
  m_o_n_quality_nonreal_taus_measure = 0;
  
  for (const SFile& sf: id.GetSFileIn()) {
    m_input_files.push_back(sf.file);
  }
  
  return;

}

void Measure::EndInputData( const SInputData& ) throw( SError ) {
  
  if (m_o_M_t) {
    delete m_o_M_t;
  }
  if (m_o_tau_pt_1prong) {
    delete m_o_tau_pt_1prong;
  }
  if (m_o_tau_pt_3prong) {
    delete m_o_tau_pt_3prong;
  }
   if (m_o_is_quality) {
    delete m_o_is_quality;
  }
    
  if (m_o_is_quality_measure) {
    delete m_o_is_quality_measure;
  }
  if (m_o_is_elec_matched) {
    delete m_o_is_elec_matched;
  }
  if (m_o_is_muon_matched) {
    delete m_o_is_muon_matched;
  }
  m_o_M_t 		    = 0;
  m_o_tau_pt_1prong	    = 0;
  m_o_tau_pt_3prong 	    = 0;
  m_o_M_eff 		    = 0.;
  m_o_delta_eta 	    = 0.;
  m_o_delta_phi 	    = 0.;
  m_o_n_quality_taus_measure = 0;
  
  m_o_n_quality_nonreal_taus_measure = 0;
 
  m_o_is_elec_matched	    = 0;
  m_o_is_muon_matched	    = 0;
  
  std::cout << "1-prong: " << m_count_1prong << "; matches with 1prong: " << dummy1_2 << "; ratio: " << dummy1_2 / m_count_1prong << std::endl;
  std::cout << "3-prong: " << m_count_3prong << "; Matches with 3prong: " << dummy3_2 << "; ratio: ";
  if (m_count_3prong > 0)
    std::cout << dummy3_2 / m_count_3prong << std::endl;
  std::cout << "promotable (anti-)matches with 1-prong: " << counter1 << "; promotable (anti-)matches with 3-prong: "  << counter3 << std::endl;
  
  std::ofstream CountedFaketypes;
  if (m_c_isunmatched == true) {
    CountedFaketypes.open("FaketypeCount_UNMATCHED.txt");
  } else {
    if (m_c_ismatched) {
      CountedFaketypes.open("FaketypeCount_MATCHED.txt");
    } else {
      CountedFaketypes.open("FaketypeCount_ANTIMATCHED.txt");
    }
  }
  for (uint idx = 0; idx < faketype_counter_1prong.size(); ++idx) {
    CountedFaketypes << std::setw(7) << faketype_counter_1prong.at(idx) << std::setw(12) << faketype_counter_3prong.at(idx) << std::setw(12) << faketype_counter_1prong_d.at(idx) << std::setw(12) << faketype_counter_3prong_d.at(idx) << std::endl;
  }
  CountedFaketypes.close();
  
  m_logger << INFO << "Events (selected only): " << event_nr << "; cont Taus: " << tau_nr << "; fakes " << fake_nr << SLogger::endmsg;
  
  return;



}
void Measure::FinalizeEfficiencies(const TString& name_u, const TString& name_d, const TString& name)  {
  
//   THeff* h_effs;
//   
//   h_effs->h_u   = Retrieve<THeff>("h_eff_u" + varname, "Efficiency");
//   h_effs->h_d   = Retrieve<THeff>("h_eff_d" + varname, "Efficiency");
//   h_effs->h_eff = Retrieve<THeff>("h_eff"   + varname, "Efficiency");
//   
//   m_logger << ::INFO << "Computing efficiency in " << name << " from " << h_effs->h_u->GetEntries() << " / " << h_effs->h_d->GetEntries() << " entries." << SLogger::endmsg;
//   
//   h_effs->h_eff->Divide(h_effs->h_u, h_effs->h_d);  
  
}


void Measure::FinalizeHistograms(TEffs* h_effs, const TString& varname)  {
  
  // TODO: reorganize h_effs to only contain h_u, h_d, h_eff and make use of FinalizeEfficiencies instead of repeating code
  
  h_effs->h_u   = Retrieve<THeff>("h_eff_u" + varname, "Efficiency");
  h_effs->h_d   = Retrieve<THeff>("h_eff_d" + varname, "Efficiency");
  h_effs->h_eff = Retrieve<THeff>("h_eff"   + varname, "Efficiency");
  
  m_logger << ::INFO << "Computing efficiency in " << "h_eff" << varname 
           << " from " << h_effs->h_u->GetEntries() << " / " << h_effs->h_d->GetEntries() << " entries." << SLogger::endmsg;
  h_effs->h_eff->Divide(h_effs->h_u, h_effs->h_d);

  h_effs->h_met_u   = Retrieve<THeff>("h_eff_met_u" + varname, "Efficiency");
  h_effs->h_met_d   = Retrieve<THeff>("h_eff_met_d" + varname, "Efficiency");
  h_effs->h_met_eff = Retrieve<THeff>("h_eff_met"   + varname, "Efficiency");
  
  m_logger << ::INFO << "Computing efficiency in " << "h_eff_met" << varname 
           << " from " << h_effs->h_met_u->GetEntries() << " / " << h_effs->h_met_d->GetEntries() << " entries." << SLogger::endmsg;
  h_effs->h_met_eff->Divide(h_effs->h_met_u, h_effs->h_met_d);
  
  // pt only
  h_effs->h_ptonly_u    = Retrieve<TH1F>("h_eff_ptonly_u" + varname, "Efficiency");
  h_effs->h_ptonly_d    = Retrieve<TH1F>("h_eff_ptonly_d" + varname, "Efficiency");
  h_effs->h_ptonly_eff  = Retrieve<TH1F>("h_eff_ptonly" + varname, "Efficiency");
  
  h_effs->h_ptonly_eff->Divide(h_effs->h_ptonly_u, h_effs->h_ptonly_d);
  
  // met only
  h_effs->h_metonly_u    = Retrieve<TH1F>("h_eff_metonly_u" + varname, "Efficiency");
  h_effs->h_metonly_d    = Retrieve<TH1F>("h_eff_metonly_d" + varname, "Efficiency");
  h_effs->h_metonly_eff  = Retrieve<TH1F>("h_eff_metonly" + varname, "Efficiency");
  
  h_effs->h_metonly_eff->Divide(h_effs->h_metonly_u, h_effs->h_metonly_d);
  
//   if (m_secondaxis == kFakeType) {
//     
//       // Slices for Jet-Fakes
//     h_effs->h_eff->ProjectionX("h_eff_qjetsonly" + varname, 5, 5);
//     h_effs->h_eff->ProjectionX("h_eff_gjetsonly" + varname, 6, 6);
//     h_effs->h_eff->ProjectionX("h_eff_hfonly" + varname, 2, 2);
//     
//     h_effs->h_qjetsonly_eff    = Retrieve<TH1D>("h_eff_qjetsonly" + varname, "Efficiency");
//     h_effs->h_gjetsonly_eff    = Retrieve<TH1D>("h_eff_gjetsonly" + varname, "Efficiency");
//     h_effs->h_hfonly_eff       = Retrieve<TH1D>("h_eff_hfonly"    + varname, "Efficiency");
//     h_effs->h_jetsonly_eff     = Retrieve<TH1D>("h_eff_jetsonly"  + varname, "Efficiency");
//     
//     
//     
//     double weight_jet_sum = 0.;
//     double weight_qjets = 0.;
//     double weight_gjets = 0.;
//     double weight_hf = 0.;
//     if (varname == "_1prong") {
//       weight_jet_sum = faketype_counter_1prong.at(2) + faketype_counter_1prong.at(5) + faketype_counter_1prong.at(6);
//       weight_qjets = faketype_counter_1prong.at(5) / weight_jet_sum;
//       weight_gjets = faketype_counter_1prong.at(6) / weight_jet_sum;
//       weight_hf = faketype_counter_1prong.at(2) / weight_jet_sum;
//     } else if (varname == "_3prong") {
//       weight_jet_sum = faketype_counter_3prong.at(2) + faketype_counter_3prong.at(5) + faketype_counter_3prong.at(6);
//       weight_qjets = faketype_counter_3prong.at(5) / weight_jet_sum;
//       weight_gjets = faketype_counter_3prong.at(6) / weight_jet_sum;
//       weight_hf = faketype_counter_3prong.at(2) / weight_jet_sum;
//     }
//     
//     h_effs->h_jetsonly_eff->Add(h_effs->h_qjetsonly_eff, h_effs->h_gjetsonly_eff, weight_qjets, weight_gjets);
//     h_effs->h_jetsonly_eff->Add(h_effs->h_jetsonly_eff, h_effs->h_hfonly_eff, 1., weight_hf);
//     
//     // Slices for electron-fakes
//     h_effs->h_eff->ProjectionX("h_eff_eleconly" + varname, 4, 4);
//     h_effs->h_eff->ProjectionX("h_eff_convonly" + varname, 1, 1);
//     
//     h_effs->h_eleconly_eff    = Retrieve<TH1D>("h_eff_eleconly" + varname, "Efficiency");
//     h_effs->h_convonly_eff    = Retrieve<TH1D>("h_eff_convonly" + varname, "Efficiency");
//     h_effs->h_electronsonly_eff    = Retrieve<TH1D>("h_eff_electronsonly" + varname, "Efficiency");
//     
//     
//     
//     double weight_electron_sum = 0.;
//     double weight_elec = 0.;
//     double weight_conv = 0.;
//     if (varname == "_1prong") {
//       weight_electron_sum = faketype_counter_1prong.at(4) + faketype_counter_1prong.at(1);
//       weight_elec = faketype_counter_1prong.at(4) / weight_electron_sum;
//       weight_conv = faketype_counter_1prong.at(1) / weight_electron_sum;
//     } else if (varname == "_3prong") {
//       weight_electron_sum = faketype_counter_3prong.at(4) + faketype_counter_3prong.at(1);
//       weight_elec = faketype_counter_3prong.at(4) / weight_electron_sum;
//       weight_conv = faketype_counter_3prong.at(1) / weight_electron_sum;
//     }
//     
//     h_effs->h_electronsonly_eff->Add(h_effs->h_eleconly_eff, h_effs->h_convonly_eff, weight_elec, weight_conv);
//   }
  
}


void Measure::EndMasterInputData(const SInputData& id) throw(SError) {

  uint nprong = 1;
  if (m_c_istaunu) {
    for (TEffs*& h_effs: h_vec_effs) {
      FinalizeHistograms(h_effs, Form("_%dprong", nprong));
      nprong = 3;
    }
  } else {
    std::string match = "matched";
    uint sel = 0;
    for (TEffs*& h_effs: h_vec_effs) {
      FinalizeHistograms(h_effs, Form("_%dprong_%s", nprong, match.c_str()));
      ++sel;
      match = "antimatched";
      if (sel == 2) {
	nprong = 3;
	match = "matched";
      }
    }
  }
  
  // additional kinematic distributions
// //   h_M_eff 	= Hist("h_M_eff");
// //   h_M_t		= Hist("h_M_t");
// //   h_delta_phi 	= Hist("h_delta_phi");
// //   h_delta_eta 	= Hist("h_delta_eta");
// //   h_pt_1prong	= Hist("h_pt_1prong");
// //   h_pt_3prong	= Hist("h_pt_3prong");

//   // write input list to file -- does not work
//   TList list0;
//   list0.SetName("InputFileList");
//   Book( list0, "InputFiles" );
//   TList* list = Retrieve<TList>("InputFileList", "InputFiles");
//   for (const TString& fn: m_input_files) {
//     TObjString fno(fn);
//     list->Add(&fno);
//   }
//   m_logger << INFO << "List with " << list->GetSize() << " entries of input files added to output." << SLogger::endmsg;

}


void Measure::BeginInputFile( const SInputData& ) throw( SError ) {

  //
  // Connect the input variables:
  //
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_pt"                   , m_tau_pt                   );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_quality"              , m_tau_quality              );

  ConnectVariable( m_c_recoTreeName.c_str(), "tau_prongness"            , m_tau_prongness            );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_phi"                  , m_tau_phi                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "tau_eta"                  , m_tau_eta                  );
 //   ConnectVariable( m_c_recoTreeName.c_str(), "is_container_20"          , m_is_container_20          ); // FIXME: add back 
  ConnectVariable( m_c_recoTreeName.c_str(), "is_container"             , m_is_container             );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_loose"                 , m_is_loose                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "is_medium"                , m_is_medium                );

  ConnectVariable( m_c_recoTreeName.c_str(), "is_true"                  , m_is_true                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "faketype"                 , m_faketype                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "faketype_full"            , m_faketype_full            );
 ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_taus"              , m_n_cont_taus              );
 //   ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_taus_20"           , m_n_cont_taus_20           );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_true_taus"              , m_n_true_taus              );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_taus"            , m_n_medium_taus            );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_quality_taus"            , m_n_quality_taus            );
 
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_loose_taus"            , m_n_loose_taus            );

   ConnectVariable( m_c_recoTreeName.c_str(), "n_cont_nonreal_taus"      , m_n_cont_nonreal_taus      );
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_medium_nonreal_taus"    , m_n_medium_nonreal_taus    );
  
//   ConnectVariable( m_c_recoTreeName.c_str(), "n_quality_nonreal_taus"    , m_n_quality_nonreal_taus    );

  ConnectVariable( m_c_recoTreeName.c_str(), "evt_MET"                  , m_met_mod                  );
 //   ConnectVariable( m_recoTreeName.c_str(), "evt_MT2"                  , m_evt_MT2                  );
 //   ConnectVariable( m_recoTreeName.c_str(), "evt_M_eff"                , m_evt_M_eff                );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_xSec"          , m_evt_weight_xSec          );

  // Mind different weights
   ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight"             , m_event_weight             );
   ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_complete_bline", m_evt_weight_complete_bline);
   ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_complete_bline", m_event_weight             );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_weight_nom"           , m_event_weight             );  // sollte identisch mit sys_nom_sig sein
//   ConnectVariable( m_recoTreeName.c_str(), "met_phi"                  , m_met_phi                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "mcid"                     , m_mcid                     );
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_MET_phi"              , m_evt_MET_phi);
  ConnectVariable( m_c_recoTreeName.c_str(), "evt_eventNumber"          , m_evt_eventNumber          );
  ConnectVariable( m_c_recoTreeName.c_str(), "electron_n"               , m_electron_n               );
  ConnectVariable( m_c_recoTreeName.c_str(), "electron_pt"              , m_electron_pt              );
  ConnectVariable( m_c_recoTreeName.c_str(), "electron_phi"             , m_electron_phi             );
  ConnectVariable( m_c_recoTreeName.c_str(), "electron_eta"             , m_electron_eta             );
  ConnectVariable( m_c_recoTreeName.c_str(), "muon_n"                   , m_muon_n                   );
  ConnectVariable( m_c_recoTreeName.c_str(), "muon_pt"                  , m_muon_pt                  );
  ConnectVariable( m_c_recoTreeName.c_str(), "muon_phi"                 , m_muon_phi                 );
  ConnectVariable( m_c_recoTreeName.c_str(), "muon_eta"                 , m_muon_eta                 );
  if (!m_c_requiretrigger.empty())
    ConnectVariable( m_c_recoTreeName.c_str(), ("trig_" + m_c_requiretrigger).c_str(), m_triggerdecision);

  //
  // Declare the output variables: (needs to be done after ConnectVariable, for those branches at least which should be copied)
  //
//   DeclareVariable( *m_tau_pt,                "tau_pt"               );
   DeclareVariable( *m_tau_quality,             "tau_quality"               );

  //   DeclareVariable( *m_tau_prongness,         "tau_prongness"        );
//   DeclareVariable( *m_tau_phi,               "tau_phi"              );
//   DeclareVariable( *m_tau_eta,               "tau_eta"              );
// //   DeclareVariable( *m_is_container_20,       "is_container_20"      ); // FIXME: add back and sort all lines as above
//   DeclareVariable( *m_is_container,          "is_container"         );
    DeclareVariable( *m_is_loose,              "is_loose"             );
//   DeclareVariable( *m_is_medium,             "is_medium"            );
//   DeclareVariable( *m_is_true,               "is_true"              );
//   DeclareVariable( *m_faketype,              "faketype"             );
//   DeclareVariable( *m_faketype_full,         "faketype_full"        );
//   DeclareVariable(  m_n_cont_taus,           "n_cont_taus"          );
// //   DeclareVariable(  m_n_cont_taus_20,        "n_cont_taus_20"       );
//   DeclareVariable(  m_n_true_taus,           "n_true_taus"          );
//   DeclareVariable(  m_n_medium_taus,         "n_medium_taus"        );
  DeclareVariable(  m_n_cont_nonreal_taus,   "n_cont_nonreal_taus"  );
//   DeclareVariable(  m_n_medium_nonreal_taus, "n_medium_nonreal_taus");
   DeclareVariable(  m_met_mod,               "evt_MET"              ); 
   
// //   DeclareVariable( *m_evt_MT2,               "evt_MT2"              );
// //   DeclareVariable( *m_evt_M_eff,             "evt_M_eff"            );
//   DeclareVariable(  m_n_baseline_el,         "n_baseline_el"        );
//   DeclareVariable(  m_n_baseline_mu,         "n_baseline_mu"        );
//   DeclareVariable(  m_pass_2eventcleaning,   "pass_2eventcleaning"  );
//   DeclareVariable(  m_pass_2baselineleptons, "pass_2baselineleptons");
//   DeclareVariable(  m_idx_promoted_ctau,     "idx_promoted_ctau"    );
//   DeclareVariable(  m_met_phi,               "met_phi"              );
//   DeclareVariable(  m_evt_weight_xSec,       "evt_weight_xSec"      );
// //   DeclareVariable(  m_evt_weight_complete_bline, "evt_weight_complete_bline");
//   DeclareVariable(  m_mcid,                  "mcid"                 );
//   DeclareVariable(  m_evt_MET_phi, 	     "evt_MET_phi"          );
//   DeclareVariable(  m_electron_n, 	     "electron_n"           );
//   DeclareVariable(  m_muon_n, 	             "muon_n"               );
//   DeclareVariable(  m_evt_eventNumber,       "evt_eventNumber"      );
    
  // outputs generated here 
  DeclareVariable( *m_o_is_quality,             "is_quality"               );
  
  DeclareVariable(  m_event_weight,          "event_weight"         );
  DeclareVariable(  m_o_is_selected_measure,           "is_selected_measure"          );
  DeclareVariable(  m_o_M_eff,                         "M_eff"                        );
  DeclareVariable( *m_o_M_t,                           "M_t"                          );
  DeclareVariable(  m_o_delta_phi,                     "delta_phi"                    );
  DeclareVariable(  m_o_delta_eta,                     "delta_eta"                    );
//   DeclareVariable( *m_o_tau_pt_1prong,                 "tau_pt_1prong"                );
//   DeclareVariable( *m_o_tau_pt_3prong,                 "tau_pt_3prong"                );
  DeclareVariable(  m_o_n_quality_taus_measure,         "n_quality_taus_measure"        );
  DeclareVariable(  m_o_n_quality_nonreal_taus_measure, "n_quality_nonreal_taus_measure");
  DeclareVariable( *m_o_is_quality_measure,             "is_quality_measure"            );
  DeclareVariable( *m_o_is_elec_matched,               "is_elec_matched"              );
  DeclareVariable( *m_o_is_muon_matched,               "is_muon_matched"              );
  
  DeclareVariable(  m_o_quality_tau0_pt_measure,        "quality_tau0_pt_measure"              );
  DeclareVariable(  m_o_quality_tau1_pt_measure,        "quality_tau1_pt_measure"              );
 
  return;

}


bool Measure::Is_tau_quality(int tau_index){
   if (((*m_tau_quality)[tau_index] >= m_c_selecttautype) && (*m_is_loose)[tau_index]){   // use condition m_is_loose to make sure baseline is added
//  if ((*m_tau_quality)[tau_index] >= m_c_selecttautype){
    return true;
  } else {
    return false;
  }
    
}

// 

void Measure::ParseConfiguration() {

  /// parse options
  /// MEAsure, PROmote, FUll faketype binning, FAKEtype binning, CONTainer tau binning, LEADing tau
  
  if (m_c_secondaxis.empty())
    m_secondaxis = kSANone;
  else if (m_c_secondaxis.find("fu") != std::string::npos)
    m_secondaxis = kFakeTypeFull;
  else if (m_c_secondaxis.find("fake") != std::string::npos)
    m_secondaxis = kFakeType;
  else if (m_c_secondaxis.find("cont1reco") != std::string::npos)
    m_secondaxis = knContTau1Reco;
  else if (m_c_secondaxis.find("cont") != std::string::npos)
    m_secondaxis = knContTau;
  else if (m_c_secondaxis.find("met") != std::string::npos)
    m_secondaxis = knMET;
  else if (m_c_secondaxis.find("prong") != std::string::npos)
    m_secondaxis = knProng;
  else
    throw("unknown");

  m_selectphd = kRandom;
//   if (option.find("lead") != std::string::npos)
//     m_selectphd = kLeading;
  
//   if (option.find("selev") != std::string::npos) {
//     int n = option.Index("selev") + 5;
//     m_selectevents = atoi(option(n, 5).Data());
//   }
  
}


bool Measure::IsEventSelected() {

  if (!m_c_requiretrigger.empty())
    if (!m_triggerdecision)
      return false;
  
  bool m_result = false;
  
  if (m_c_select1tauevents) {
    m_result = false;
    for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
      if (Is_tau_quality(idx) && (*m_is_true)[idx]) {
        m_result = true;
        break;
      }   
    }
  } else 
    m_result = true;
   
  return m_result;
  
}


int Measure::GetSecondAxisBin(int tau_index) {
  
  // NOTE: 
  // This does not actually return the bin number as in ROOT counting (0 = underflow), but the number to be used to pick the bin on the 2nd axis 
  // (if it returns 0, use bin 1 on an axis going from -0.5 to something)
  // The exception is the MET binning where just the (capped) MET value is returned and a matching binning is provided in InitEff().
  int binno = 0;
  switch (m_secondaxis) {
    case kFakeType:
      binno = (*m_faketype)[tau_index];
      break;
    case kFakeTypeFull:
      binno = (*m_faketype_full)[tau_index];
      break;
    case knContTau:
      binno = m_n_cont_taus;
      break;
    case knContTau1Reco:
      exit(1);
//       if (Nrealreco() == 0)
//         binno = 0; // can reuse bin 0 in these cases (because for >=1 reco tau, bins 1.. are used: whenever there is a tau to be measured, n_cont_taus will be at least 1)
//       else
//         binno = m_n_cont_taus;
      break;
    case knMET:
      binno = m_met_mod;
      if (binno > m_c_bin_maxmet-1) // cap at m_c_maxmet-1
        binno = m_c_bin_maxmet-1;
      break;
    case kSANone:
      binno = 0;
      break;
    case knProng:
      binno = (*m_tau_prongness)[tau_index];
      break;
  }    
  return binno;
  
}


int Measure::GetThirdAxisBin(int tau_index) {
  
  // return corresponding index of vector h_vec_effs
  int nprong = (*m_tau_prongness)[tau_index];
  if (m_c_istaunu) {		// no matching for taunu sample
    if (nprong == 1)
      return 0;
    if (nprong == 3)
      return 1;
  } else {
    bool match;
    if (m_c_isenu) {
      match = (*m_o_is_elec_matched)[tau_index];
      if (nprong == 1 && match == true) 
	return 0;
      if (nprong == 1 && match == false)
	return 1;
      if (nprong == 3 && match == true)
	return 2;
      if (nprong == 3 && match == false)
	return 3;
    } else if (m_c_ismunu) {
      match = (*m_o_is_muon_matched)[tau_index];
      if (nprong == 1 && match == true) 
	return 0;
      if (nprong == 1 && match == false)
	return 1;
      if (nprong == 3 && match == true)
	return 2;
      if (nprong == 3 && match == false)
	return 3;
    }
  }
  m_logger << ::WARNING << "GetThirdAxisBin: Unexpected prongness " << nprong << SLogger::endmsg;
  return 0;
  
}

void Measure::FindMatch(std::vector<double> *pt, std::vector<TLorentzVector> *LorentzVec, std::vector<bool> *match) {
  double deltaR;
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    bool found_match = false;
    for (uint idy = 0; idy < pt->size(); ++idy) {
      deltaR = ((*m_o_tau_LorentzVec)[idx]).DeltaR((*LorentzVec)[idy]);
      if (deltaR < 0.2) {
	found_match = true;
	break;
      }
    }
    (*match).push_back(found_match);
    if ((*match)[idx] ==  true) {
      if ((*m_tau_prongness)[idx] == 1) {
	++dummy1_2;
      } else if ((*m_tau_prongness)[idx] == 3) {
	++dummy3_2;
      }
    }
  }
}

void Measure::ComputeLorentzVec (std::vector<TLorentzVector> *LorentzVec ,std::vector<double> *pt, std::vector<double> *eta, std::vector<double> *phi, double m) {
  
  for (uint idx = 0; idx < pt->size(); ++idx) {
    TLorentzVector TLV;
    TLV.SetPtEtaPhiM((*pt)[idx], (*eta)[idx], (*phi)[idx], m);
    LorentzVec->push_back(TLV);
  }
}

void Measure::CountFaketypes (std::vector<int>& counter, uint idx) {
  ++counter.at((*m_faketype)[idx]);
}

void Measure::FillHistogramsD (int tau_index, TEffs* hist) {
  hist->h_d->Fill((*m_tau_pt)[tau_index], GetSecondAxisBin(tau_index), m_event_weight);
  hist->h_met_d->Fill(m_met_mod, GetSecondAxisBin(tau_index), m_event_weight);
  hist->h_ptonly_d->Fill((*m_tau_pt)[tau_index], m_event_weight);
  if ((*m_tau_pt)[tau_index] > m_c_meanfakeeff_tauptcutoff) {
    hist->h_metonly_d->Fill(m_met_mod, m_event_weight);
  }
}

void Measure::FillHistogramsU (int tau_index, TEffs* hist) {
  hist->h_u->Fill((*m_tau_pt)[tau_index], GetSecondAxisBin(tau_index), m_event_weight);
  hist->h_met_u->Fill(m_met_mod, GetSecondAxisBin(tau_index), m_event_weight);
  hist->h_ptonly_u->Fill((*m_tau_pt)[tau_index], m_event_weight);
  if ((*m_tau_pt)[tau_index] > m_c_meanfakeeff_tauptcutoff) {
    hist->h_metonly_u->Fill(m_met_mod, m_event_weight);
  }
}


void Measure::DoMeasure() {  
  
//   std::sort(m_tau_pt->begin(), m_tau_pt->end(), std::greater<double>());
//  std::cout << "Loose: "<< m_c_selectslooseinsteadofmedium << std::endl;
//    m_logger << ::DEBUG << "TEST: DoMeasure1" << SLogger::endmsg;
  if (m_c_isenu == true || m_c_ismunu == true) {
    double tau_mass = 1776.82;
    double mu_mass = 105.66;
    double e_mass = 0.511;
    ComputeLorentzVec(m_o_tau_LorentzVec, m_tau_pt, m_tau_eta, m_tau_phi, tau_mass);
    if (m_c_isenu) {
      ComputeLorentzVec(m_o_electron_LorentzVec, m_electron_pt, m_electron_eta, m_electron_phi, e_mass);
      FindMatch(m_electron_pt, m_o_electron_LorentzVec, m_o_is_elec_matched);
    } else if (m_c_ismunu) {
      ComputeLorentzVec(m_o_muon_LorentzVec, m_muon_pt, m_muon_eta, m_muon_phi, mu_mass);
      FindMatch(m_muon_pt, m_o_muon_LorentzVec, m_o_is_muon_matched);
    }
  }
  
  uint pickme = -1;
  if (m_selectphd == kLeading) {
//     pickme = std::distance(ptcopy->begin(), std::max_element(ptcopy->begin(), ptcopy->end()));
    pickme = 0;
    for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
      if (m_is_true->at(idx))
        continue;
      if (!m_is_container->at(idx))
        continue;
      if ((*m_tau_pt)[idx] > (*m_tau_pt)[pickme])
        pickme = idx;
    }
  }
  
  double abs_tauE = 0;			// absolute value of pt for event
  
//   std::cout << "tau_pt size: " << m_tau_pt->size() << std::endl;
  
  (*m_o_is_quality_measure).resize(m_tau_pt->size());
for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    (*m_o_is_quality_measure)[idx] = Is_tau_quality(idx);
}
   

   
   
  ++event_nr;
  
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    
    ++tau_nr;
    
    abs_tauE += (*m_tau_pt)[idx] * (*m_tau_pt)[idx];  // computation can't be done in same loop as use
//     std::cout << "tau_pt: " << (*m_tau_pt)[idx] << "; GetSecondAxisBin: " << GetSecondAxisBin(idx) << "; event_weight: " << m_event_weight << std::endl;
//     std::cout << "true? " << m_is_true->at(idx) << std::endl;
    
    if (m_is_true->at(idx))
      continue;
    if (!m_is_container->at(idx))
      continue;
    if ((*m_tau_pt)[idx] < m_c_mintaupt)
      continue;
    if (!m_c_isunmatched) {
      if (m_c_isenu) {
//       if ((*m_tau_prongness)[idx] == 1 || (*m_tau_prongness)[idx] == 3)
	if (m_c_ismatched_sign) {
	  if (m_c_ismatched) {
	    if (!m_o_is_elec_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (m_o_is_elec_matched->at(idx)) {
	      continue;
	    }
	  }
	} else {
	  if (m_c_ismatched) {
	    if (m_o_is_elec_matched->at(idx)) {
	      if ((*m_tau_prongness)[idx] == 1) {
		++counter1;
	      } else if ((*m_tau_prongness)[idx]) {
		++counter3;
	      }
	      continue;
	    }
	  } else {
	    if (!m_o_is_elec_matched->at(idx)) {
	      if ((*m_tau_prongness)[idx] == 1) {
		++counter1;
	      } else if ((*m_tau_prongness)[idx]) {
		++counter3;
	      }
	      continue;
	    }
	  }
	}
      }
      if (m_c_ismunu) {
//       if ((*m_tau_prongness)[idx] == 1 || (*m_tau_prongness)[idx] == 3)
	if (m_c_ismatched_sign) {
	  if (m_c_ismatched) {
	    if (!m_o_is_muon_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (m_o_is_muon_matched->at(idx)) {
	      continue;
	    }
	  }
	} else {
	  if (m_c_ismatched) {
	    if (m_o_is_muon_matched->at(idx)) {
	      continue;
	    }
	  } else {
	    if (!m_o_is_muon_matched->at(idx)) {
	      continue;
	    }
	  }
	}
      }
    }
    if (m_selectphd == kLeading) 
      if (idx != pickme)
        continue;
    
    TEffs* h_effs = h_vec_effs[GetThirdAxisBin(idx)];
//     h_effs->h_d->Fill((*m_tau_pt)[idx], GetSecondAxisBin(idx), m_event_weight);
//     h_effs->h_met_d->Fill(m_met_mod, GetSecondAxisBin(idx), m_event_weight);
//     h_effs->h_ptonly_d->Fill((*m_tau_pt)[idx], m_event_weight);
//     if ((*m_tau_pt)[idx] > m_c_meanfakeeff_tauptcutoff) {
//       h_effs->h_metonly_d->Fill(m_met_mod, m_event_weight);
//     }
    FillHistogramsD(idx, h_effs);
    if ((*m_tau_prongness)[idx] == 1) {
      CountFaketypes(faketype_counter_1prong_d, idx);
    } else if ((*m_tau_prongness)[idx] == 3) {
      CountFaketypes(faketype_counter_3prong_d, idx);
    }
    
  
//     std::cout << "tau_pt: " << (*m_tau_pt)[idx] << "; GetSecondAxisBin: " << GetSecondAxisBin(idx) << "; event_weight: " << m_event_weight << std::endl;
    
    if (Is_tau_quality(idx)) {
//       h_effs->h_u->Fill((*m_tau_pt)[idx], GetSecondAxisBin(idx), m_event_weight);
//       h_effs->h_met_u->Fill(m_met_mod, GetSecondAxisBin(idx), m_event_weight);
//       h_effs->h_ptonly_u->Fill((*m_tau_pt)[idx], m_event_weight);
//       if ((*m_tau_pt)[idx] > m_c_meanfakeeff_tauptcutoff) {
// 	h_effs->h_metonly_u->Fill(m_met_mod, m_event_weight);
//       }
      FillHistogramsU(idx, h_effs);
      ++fake_nr;
      if ((*m_tau_prongness)[idx] == 1) {
	CountFaketypes(faketype_counter_1prong, idx);
      } else if ((*m_tau_prongness)[idx] == 3) {
	CountFaketypes(faketype_counter_3prong, idx);
      }
//       std::cout << "loose tau_pt: " << (*m_tau_pt)[idx] << std::endl;
    }
  
  
  abs_tauE = sqrt(abs_tauE);
  
  Float_t quality_tau0_pt_measure = -1.;
  Float_t quality_tau1_pt_measure = -1.;
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    
    if (Is_tau_quality(idx)) {
      if((*m_tau_pt)[idx] > m_c_mintaupt) {
        ++m_o_n_quality_taus_measure;
        if((*m_is_true)[idx] == false) {
          ++m_o_n_quality_nonreal_taus_measure;
        }
      } else {
        (*m_o_is_quality_measure)[idx] = false;
      }
      // find (sub-)leading tau pt
      if ((*m_tau_pt)[idx] > quality_tau0_pt_measure) {
        quality_tau1_pt_measure = quality_tau0_pt_measure;
        quality_tau0_pt_measure = (*m_tau_pt)[idx];
      } else if ((*m_tau_pt)[idx] > quality_tau1_pt_measure) {
        quality_tau1_pt_measure = (*m_tau_pt)[idx];
      }
    }
    
    
    // fit vector to correct size
    m_o_M_t->push_back(0);
    (*m_o_M_t)[idx] = sqrt(2*abs_tauE*m_met_mod*(1-cos(m_evt_MET_phi - (*m_tau_phi)[idx])));	// transverse mass; computed in own loop as it needs abs_tauE as well as the original tau_pt vector
    
    if (m_tau_prongness->at(idx) == 1) {
      m_o_tau_pt_1prong->push_back(m_tau_pt->at(idx));
      ++m_count_1prong;
    }
    if (m_tau_prongness->at(idx) == 3) {
      m_o_tau_pt_3prong->push_back((*m_tau_pt)[idx]);
      ++m_count_3prong;
    }
  }
  
  m_o_quality_tau0_pt_measure = quality_tau0_pt_measure;
  m_o_quality_tau1_pt_measure = quality_tau1_pt_measure;
  
  
  // FIXME: this doesn't respect is_medium!
/*  
  // M_eff, delta_eta, delta_phi need sorted pt vector; therefore clone and sort clone
  std::vector<float> *pt_helper = new std::vector<float>(m_tau_pt->begin(), m_tau_pt->end()); 
  std::sort(pt_helper->begin(), pt_helper->end(), std::greater<double>());
  
  
  // Fill output branches without selection
  m_o_M_eff = m_met_mod;					// effective mass
  if (m_tau_pt->size() >= 1) {
    m_o_M_eff += (*pt_helper)[0];
  }
  if (m_tau_pt->size() >= 2) {
    m_o_M_eff += (*pt_helper)[1];
  }*/
  //TODO: implement correct computation of deltas; currently no consistency between sorted pt vector and unsorted is_true vector
//   int count = 0;
//   
//   for (uint idx = 0; idx < pt_helper->size(); ++idx) {
//     
// //     m_logger << ::DEBUG << m_evt_eventNumber << "; pt_helper: " << (*pt_helper)[idx] << "; tau_pt: " << (*m_tau_pt)[idx] << SLogger::endmsg;
//     
//     if (m_is_true->at(idx) && count == 0) {
//       
//       // catch leading tau as reference, if leading tau is selected catch subleading instead
//       int which_tau = 0;
//       if (idx == 0) {
// 	which_tau = 1;
//       }
//       m_o_delta_phi = (*m_tau_phi)[idx] - (*m_tau_phi)[which_tau];			// azimutal angle
//       m_o_delta_eta = (*m_tau_eta)[idx] - (*m_tau_eta)[which_tau];			// pseudorapidity
//       ++count;
//     }   
//   }
//   
//   m_logger << ::DEBUG << "real meds: " << m_o_n_medium_taus_measure << SLogger::endmsg;
//   m_logger << ::DEBUG << "new nonreal meds: " << m_o_n_medium_nonreal_taus_measure << SLogger::endmsg;
//   m_logger << ::DEBUG << "old nonreal meds: " << m_n_medium_nonreal_taus << SLogger::endmsg;
//   
  



}}


void Measure::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {

//   m_logger << ::WARNING << "Event" << m_tau_pt << " " << m_tau_pt->size() << SLogger::endmsg;
  
  // make sure, variables are cleared after each event
  m_o_M_t->clear();
  m_o_tau_pt_1prong->clear();
  m_o_tau_pt_3prong->clear();
  m_o_is_quality_measure->clear();
  m_o_is_elec_matched->clear();
  m_o_is_muon_matched->clear();
  m_o_electron_LorentzVec->clear();
  m_o_muon_LorentzVec->clear();
  m_o_tau_LorentzVec->clear();
  m_o_M_eff = 0.;
  m_o_delta_phi = 0.;
  m_o_delta_eta = 0.;
  m_o_n_quality_taus_measure = 0;
  m_o_n_quality_nonreal_taus_measure = 0;
  m_o_quality_tau0_pt_measure        = -1.;
  m_o_quality_tau1_pt_measure        = -1.;
  m_o_is_quality->clear();

  
  (*m_o_is_quality).resize(m_tau_pt->size());
  for (uint idx = 0; idx < m_tau_pt->size(); ++idx) {
    (*m_o_is_quality)[idx] = Is_tau_quality(idx);
   } 
   
  
  if ((m_o_is_selected_measure = IsEventSelected())) {
    ++m_n_selected_events;
    if (m_c_istaunu) {
      DoMeasure();
    } else {
      if (m_electron_n >= 1 || m_muon_n >= 1) {
        DoMeasure();
      }
    }
  }

}

