// Dear emacs, this is -*- c++ -*-
// $Id: Measure.h 2686 2016-12-18 21:21:22Z eis $
#ifndef Measure_H
#define Measure_H

// SFrame include(s):
#include "core/include/SLogger.h"
#include "core/include/SCycleBase.h"
#include "core/include/SCycleBaseNTuple.h"
#include "core/include/SCycleBaseHist.h"

// ROOT includes
#include <TH1F.h>
#include <TH2F.h>
#include <TLorentzVector.h>

// other includes
#include <string>
#include <array>

// template < class T > 
// std::ostream& operator << (std::ostream& os, const std::vector<T>& v)  {
//     os << "[ ";
//     for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii) {
//       if (ii != v.begin())
//         os << ", ";
//       os << *ii;
//     }
//     os << " ]";
//     return os;
// }


/**
 *   @short Put short description of class here
 *
 *          Put a longer description over here...
 *
 *  @author Put your name here
 * @version $Revision: 2686 $
 */
class Measure : public SCycleBase {

  enum kSecondAxis {
    kSANone,
    kFakeType,
    kFakeTypeFull,
    knContTau,
    knContTau1Reco,
    knMET,
    knProng,
  };
  
  enum kSelectPhD {
    kRandom,
    kLeading
  };

  struct TEffs;
  
public:
  /// Default constructor
  Measure();
  /// Default destructor
  ~Measure();

  /// Function called at the beginning of the cycle
  virtual void BeginCycle() throw( SError );
  /// Function called at the end of the cycle
  virtual void EndCycle() throw( SError );

  /// Function called at the beginning of a new input data
  virtual void BeginInputData( const SInputData& ) throw( SError );
  /// Function called after finishing to process an input data
  virtual void EndInputData  ( const SInputData& ) throw( SError );
  virtual void EndMasterInputData(const SInputData&) throw(SError);

  /// Function called after opening each new input file
  virtual void BeginInputFile( const SInputData& ) throw( SError );

  /// Function called for every event
  virtual void ExecuteEvent( const SInputData&, Double_t ) throw( SError );

  void ParseConfiguration();
  bool IsEventSelected();
  int  GetSecondAxisBin(int tau_index);
  int  GetThirdAxisBin(int tau_index);
  void DoMeasure();
  void InitializeHistograms();
  void InitializeEffHistograms(TEffs* h_effs, const TString& varname);
  void FinalizeEfficiencies(const TString& name_u, const TString& name_d, const TString& name);
  void FinalizeHistograms(Measure::TEffs* h_effs, const TString& varname);
  void FindMatch(std::vector< double >*, std::vector< TLorentzVector >*, std::vector< bool >*);
  void ComputeLorentzVec(std::vector<TLorentzVector>*, std::vector<double>*, std::vector<double>*, std::vector<double>*, double m);
  std::vector<Float_t> SetBinning(std::vector<Float_t>, std::vector<Float_t>);
  void CountFaketypes(std::vector<int>&, uint);
  void FillHistogramsD(int, TEffs*);
  void FillHistogramsU(int, TEffs*);
  bool Is_tau_quality(int tau_index);
  
  
private:

  // efficiency measurement
  typedef TH2F THeff;
  struct TEffs {
    THeff* h_u; 
    THeff* h_d; 
    THeff* h_eff;
    //
    THeff* h_met_u; 
    THeff* h_met_d; 
    THeff* h_met_eff;
    
    TH1F* h_ptonly_u;
    TH1F* h_ptonly_d;
    TH1F* h_ptonly_eff;
  
    TH1F* h_metonly_u;
    TH1F* h_metonly_d;
    TH1F* h_metonly_eff;
    
//     TH1D* h_qjetsonly_eff;
//     TH1D* h_gjetsonly_eff;
//     TH1D* h_hfonly_eff;
//     TH1D* h_jetsonly_eff;
//     
//     TH1D* h_eleconly_eff;
//     TH1D* h_convonly_eff;
//     TH1D* h_electronsonly_eff;
  };
  
  
  std::vector<TEffs*> h_vec_effs;

  enum kSecondAxis m_secondaxis;  
  enum kSelectPhD m_selectphd;
  
  std::vector<TString> m_input_files;
 
  // Histograms
// //   TH1* h_M_eff;
// //   TH1* h_M_t;
// //   TH1* h_delta_phi;
// //   TH1* h_delta_eta;
// //   TH1* h_pt_1prong;
// //   TH1* h_pt_3prong;

  
  
  //
  // The input variables
  //
  std::vector<double>  *m_tau_pt;
  std::vector<int>     *m_tau_quality;
  
  std::vector<float>   *m_tau_prongness;
  std::vector<double>  *m_evt_MT2;
  std::vector<double>  *m_evt_M_eff;
  std::vector<double>  *m_tau_phi;
  std::vector<double>  *m_tau_eta;
  std::vector<bool>    *m_is_container;
  std::vector<bool>    *m_is_container_20;
  std::vector<bool>    *m_is_loose;
  std::vector<bool>    *m_is_medium;

  std::vector<bool>    *m_is_true;
  std::vector<int>     *m_faketype;
  std::vector<int>     *m_faketype_full;
  std::vector<double>  *m_electron_pt;
  std::vector<double>  *m_electron_phi;
  std::vector<double>  *m_electron_eta;
  std::vector<double>  *m_muon_pt;
  std::vector<double>  *m_muon_phi;
  std::vector<double>  *m_muon_eta;
  
  Int_t           m_n_cont_taus;
  Int_t           m_n_cont_taus_20;
  Int_t           m_n_true_taus;
  Int_t           m_n_medium_taus;
  Int_t           m_n_quality_taus;
  Int_t           m_n_loose_taus;
  Int_t           m_n_baseline_el;
  Int_t           m_n_baseline_mu;
  Int_t           m_n_cont_nonreal_taus;
  Int_t           m_n_medium_nonreal_taus;
//   Int_t           m_n_quality_nonreal_taus;
  Bool_t          m_pass_2eventcleaning;
  Bool_t          m_pass_2baselineleptons;
  Int_t           m_idx_promoted_ctau;
  Double_t        rel_xsec;
  Double_t        m_event_weight;     // NOTE: for mc15
  Double_t        m_evt_weight_complete_bline;
  Int_t           m_mcid;             // NOTE: for mc15
  Float_t         promote_weight;
  Double_t        m_met_mod;
  Double_t        m_met_phi;
  Double_t        m_evt_MET_phi;
  Double_t        m_evt_weight_xSec;
  Int_t           m_evt_eventNumber;
  Int_t           m_electron_n;
  Double_t        m_electron_m;
  Int_t           m_muon_n;
  bool            m_triggerdecision;
  long            m_n_selected_events; //< counting number of selected events


  //
  // The output variables
  //
  bool            m_o_is_selected_measure;
  
  Float_t 	  m_o_M_eff;
  Float_t	  m_o_delta_phi;
  Float_t 	  m_o_delta_eta;
  Float_t         m_o_quality_tau0_pt_measure;   ///< pt of leading medium tau
  Float_t         m_o_quality_tau1_pt_measure;   ///< pt of 2nd leading medium tau
 
  Int_t 	  m_o_n_quality_taus_measure;
  Int_t 	  m_o_n_quality_nonreal_taus_measure;
 
  std::vector<double>  *m_o_M_t;
  std::vector<double>  *m_o_tau_pt_1prong;
  std::vector<double>  *m_o_tau_pt_3prong;
  std::vector<bool>    *m_o_is_quality_measure;
  std::vector<bool>    *m_o_is_quality;

  std::vector<bool>    *m_o_is_elec_matched;
  std::vector<bool>    *m_o_is_muon_matched;
  std::vector<TLorentzVector>  *m_o_tau_LorentzVec;
  std::vector<TLorentzVector>  *m_o_muon_LorentzVec;
  std::vector<TLorentzVector>  *m_o_electron_LorentzVec;
  
  
  
  //
  // Configuration
  //
  static std::string m_c_recoTreeName;
  static std::string m_c_metaTreeName;
  static std::string m_c_secondaxis;
  static double m_c_bin_maxmet;
  static double m_c_mintaupt;
  static double m_c_meanfakeeff_tauptcutoff;
  static bool m_c_isenu;
  static bool m_c_ismunu;
  static bool m_c_istaunu;
  static bool m_c_ismatched;
  static bool m_c_ismatched_sign;
  static bool m_c_isunmatched;
  static bool m_c_select1tauevents;          //< only measure efficiency in events where the real tau has been reconstructed (to avoid skimming bias)
  static int m_c_selecttautype;              // 0: container, 1: loose, 2: medium, 3: tight
 static std::string m_c_requiretrigger;      //< require positive trigger decision

  // Macro adding the functions for dictionary generation
  ClassDef( Measure, 0 );

}; // class Measure

#endif // Measure_H

