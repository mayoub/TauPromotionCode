// Dear emacs, this is -*- c++ -*-
// $Id: Promote.h 2686 2016-12-18 21:21:22Z eis $
#ifndef Promote_H
#define Promote_H

// SFrame include(s):
#include "core/include/SCycleBase.h"
#include "core/include/SLogger.h"
#include "core/include/SCycleBaseNTuple.h"

// ROOT includes
#include <TH1.h>
#include <TH2F.h>
#include <TFile.h>

// other includes
#include <string>
#include <vector>


/**
 *   @short Put short description of class here
 *
 *          Put a longer description over here...
 *
 *  @author Put your name here
 * @version $Revision: 2686 $
 */
class Promote : public SCycleBase {

  enum kSecondAxis {
    kSANone,
    kFakeType,
    kFakeTypeFull,
    knContTau,
    knContTau1Reco,
    knMET,
    knProng,
  };

  enum kSelectPhD {
    kRandom,
    kLeading
  };

public:
  /// Default constructor
  Promote();
  /// Default destructor
  ~Promote();

  /// Function called at the beginning of the cycle
  virtual void BeginCycle() throw( SError );
  /// Function called at the end of the cycle
  virtual void EndCycle() throw( SError );

  /// Function called at the beginning of a new input data
  virtual void BeginInputData( const SInputData& ) throw( SError );
  /// Function called after finishing to process an input data
  virtual void EndInputData  ( const SInputData& ) throw( SError );

  /// Function called after opening each new input file
  virtual void BeginInputFile( const SInputData& ) throw( SError );

  /// Function called for every event
  virtual void ExecuteEvent( const SInputData&, Double_t ) throw( SError );

  void  ParseConfiguration();
  bool  IsEventSelected();
  int   GetSecondAxisBin(int tau_index);
  int   GetThirdAxisBin(int tau_index);
  void  DoCount();
  float GetEfficiency(int idx);
  float DoPromote();
//    void InitializeHistograms();
  void  LoadEffHistograms();

 
private:

  long higheffcounter;
  long loweffcounter;
  long negeffcounter;
  long oneeffcounter;
  long generalcounter;
  
  // efficiency measurement
  typedef TH2F THeff;
  struct TEffs {
    THeff* h_u;
    THeff* h_d;
    THeff* h_eff;
    //
    THeff* h_met_u;
    THeff* h_met_d;
    THeff* h_met_eff;
  };
  std::vector<TEffs*> h_vec_effs;

  enum kSecondAxis m_secondaxis;
  enum kSelectPhD m_selectphd;
  
  //
  // The input variables
  //
  std::vector<double>  *m_tau_pt;
  std::vector<int>     *m_tau_quality;
  
  std::vector<float>   *m_tau_prongness;
  std::vector<double>  *m_evt_MT2;
  std::vector<double>  *m_evt_M_eff;
  std::vector<double>  *m_tau_phi;
  std::vector<double>  *m_tau_eta;
  std::vector<bool>    *m_is_container;
  std::vector<bool>    *m_is_container_20;
  std::vector<bool>    *m_is_loose;
  std::vector<bool>    *m_is_medium;
  std::vector<bool>    *m_is_true;
  std::vector<int>     *m_faketype;
  std::vector<int>     *m_faketype_full;
  std::vector<double>  *m_M_t;
  std::vector<double>  *m_tau_pt_1prong;
  std::vector<double>  *m_tau_pt_3prong;
  std::vector<bool>    *m_is_quality_measure;
  std::vector<bool>    *m_is_elec_matched;
  std::vector<bool>    *m_is_muon_matched;

  Int_t           m_n_cont_taus;
  Int_t           m_n_cont_taus_20;
  Int_t           m_n_true_taus;
  Int_t           m_n_medium_taus;
  Int_t           m_n_quality_taus;
  Int_t           m_n_quality_nonreal_taus; 
  Int_t           m_n_baseline_el;
  Int_t           m_n_baseline_mu;
  Int_t           m_n_cont_nonreal_taus;
  Int_t           m_n_medium_nonreal_taus;
  Bool_t          m_pass_2eventcleaning;
  Bool_t          m_pass_2baselineleptons;
  Int_t           m_idx_promoted_ctau;
  Double_t        rel_xsec;
  Double_t        m_event_weight;     // NOTE: for mc15
  Double_t        m_evt_weight_complete_bline;
  Int_t           m_mcid;             // NOTE: for mc15
  Float_t         promote_weight;
  Double_t        m_met_mod;
  Double_t        m_met_phi;
  Double_t        m_evt_MET_phi;
  Double_t        m_evt_weight_xSec;
  bool            m_is_selected_measure;
  Double_t 	  m_M_eff;
  Double_t	  m_delta_phi;
  Double_t 	  m_delta_eta;
  Int_t           m_evt_eventNumber;
  Int_t           m_electron_n;
  Int_t           m_muon_n;
  Int_t 	  m_n_quality_taus_measure;
  bool            m_triggerdecision;
  long            m_n_selected_events; //< counting number of selected events
  bool Is_tau_quality(int tau_index);

  
  
  //
  // The output variables
  //
  Float_t            m_o_promote_weight;
  Float_t            m_o_promote_eff;              //! efficiency of promoted tau
  Float_t            m_o_combined_weight;
  bool               m_o_is_selected_promote;
  uint               m_o_nPhD;
  uint               m_o_idx_phd;
  std::vector<bool> *m_o_is_quality_promote;
  std::vector<bool> *m_o_is_quality;

  Int_t		     m_o_n_quality_taus_promote;
  Int_t 	     m_o_n_quality_nonreal_taus_promote;
  Float_t            m_o_quality_tau0_pt_promote;   ///< pt of leading [quality] tau
  Float_t            m_o_quality_tau1_pt_promote;   ///< pt of 2nd leading [quality] tau
  

  //
  // Configuration
  //
  static std::string m_c_recoTreeName;
  static std::string m_c_metaTreeName;
  static std::string m_c_secondaxis;
  static std::string m_c_efferror;
  static double m_c_bin_maxmet;
  static double m_c_mintaupt;
  static double m_c_meanfakeeff_tauptcutoff;
  static double m_c_meanfakeeff_metcutoff;
  static bool m_c_isenu;
  static bool m_c_ismunu;
  static bool m_c_istaunu;
  static bool m_c_ismatched;
  static bool m_c_ismatched_sign;
  static bool m_c_isunmatched;
  static std::string m_c_requiretrigger;     //< require positive trigger decision
  static std::string m_c_pathhisteff;        //< path to file with efficiency histograms
  static int m_c_selecttautype;              // 0: container, 1: loose, 2: medium, 3: tight


  // Macro adding the functions for dictionary generation
  ClassDef( Promote, 0 );

}; // class Promote

#endif // Promote_H

