// Dear emacs, this is -*- c++ -*-
// $Id: Promote_LinkDef.h 2605 2016-06-07 10:45:38Z eis $
#ifdef __CINT__

#include <vector>

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

// Add the declarations of your cycles, and any other classes for which you
// want to generate a dictionary, here. The usual format is:
//
// #pragma link C++ class MySuperClass+;

#pragma link C++ class Measure+;

#pragma link C++ class Promote+;

#pragma link C++ class TruthMatch+;

#endif // __CINT__
