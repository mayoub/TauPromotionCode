// Dear emacs, this is -*- c++ -*-
// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#ifndef TruthMatch_H
#define TruthMatch_H

// SFrame include(s):
#include "core/include/SCycleBase.h"

/**
 *   @short Put short description of class here
 *
 *          Put a longer description over here...
 *
 *  @author Put your name here
 * @version $Revision: 344 $
 */
class TruthMatch : public SCycleBase {

public:
  /// Default constructor
  TruthMatch();
  /// Default destructor
  ~TruthMatch();

  /// Function called at the beginning of the cycle
  virtual void BeginCycle() throw( SError );
  /// Function called at the end of the cycle
  virtual void EndCycle() throw( SError );

  /// Function called at the beginning of a new input data
  virtual void BeginInputData( const SInputData& ) throw( SError );
  /// Function called after finishing to process an input data
  virtual void EndInputData  ( const SInputData& ) throw( SError );

  /// Function called after opening each new input file
  virtual void BeginInputFile( const SInputData& ) throw( SError );

  /// Function called for every event
  virtual void ExecuteEvent( const SInputData&, Double_t ) throw( SError );

  void InitializeHistograms();

private:
  //
  // The input variables
  //
  std::vector<float>   *m_tau_pt;
  std::vector<float>   *m_tau_prongness;
  std::vector<double>  *m_evt_MT2;
  std::vector<double>  *m_evt_M_eff;
  std::vector<double>  *m_tau_phi;
  std::vector<double>  *m_tau_eta;
  std::vector<bool>    *m_is_container;
  std::vector<bool>    *m_is_container_20;
  std::vector<bool>    *m_is_loose;
  std::vector<bool>    *m_is_medium;
  std::vector<bool>    *m_is_true;
  std::vector<int>     *m_faketype;
  std::vector<int>     *m_faketype_full;
  Int_t           m_n_cont_taus;
  Int_t           m_n_cont_taus_20;
  Int_t           m_n_true_taus;
  Int_t           m_n_medium_taus;
  Int_t           m_n_baseline_el;
  Int_t           m_n_baseline_mu;
  Int_t           m_n_cont_nonreal_taus;
  Int_t           m_n_medium_nonreal_taus;
  Bool_t          m_pass_2eventcleaning;
  Bool_t          m_pass_2baselineleptons;
  Int_t           m_idx_promoted_ctau;
  Double_t        rel_xsec;
  Double_t        m_event_weight;     // NOTE: for mc15
  Double_t        m_evt_weight_complete_bline;
  Int_t           m_mcid;             // NOTE: for mc15
  Float_t         promote_weight;
  Double_t        m_met_mod;
  Double_t        m_met_phi;
  Double_t        m_evt_MET_phi;
  Double_t        m_evt_weight_xSec;

  //
  // The output variables
  //

  //
  // Configuration
  //
  std::string m_c_recoTreeName;
  std::string m_c_metaTreeName;

  // Macro adding the functions for dictionary generation
  ClassDef( TruthMatch, 0 );

}; // class TruthMatch

#endif // TruthMatch_H

